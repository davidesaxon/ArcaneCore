#include <gtest/gtest.h>

#include <arcanecore/base/str/StringOperations.hpp>

//------------------------------------------------------------------------------
//                                     TESTS
//------------------------------------------------------------------------------

TEST(StringOperations, count_occurrences)
{
    EXPECT_EQ(arc::str::count_occurrences("banana", "a"), 3);
    EXPECT_EQ(arc::str::count_occurrences("banana", "an"), 2);
    EXPECT_EQ(arc::str::count_occurrences("banana", "ana"), 1);
    EXPECT_EQ(arc::str::count_occurrences("bananana", "ana"), 2);
}

TEST(StringOperations, split)
{
    {
        std::vector<std::string> result =
            arc::str::split("/this/is//a/test", "/");
        EXPECT_EQ(result.size(), 6);
        EXPECT_EQ(result[0], "");
        EXPECT_EQ(result[1], "this");
        EXPECT_EQ(result[2], "is");
        EXPECT_EQ(result[3], "");
        EXPECT_EQ(result[4], "a");
        EXPECT_EQ(result[5], "test");
    }

    {
        std::vector<std::string> result =
            arc::str::split("^&*this^&*is^&*^&*a^&*test^&*", "^&*");
        EXPECT_EQ(result.size(), 7);
        EXPECT_EQ(result[0], "");
        EXPECT_EQ(result[1], "this");
        EXPECT_EQ(result[2], "is");
        EXPECT_EQ(result[3], "");
        EXPECT_EQ(result[4], "a");
        EXPECT_EQ(result[5], "test");
        EXPECT_EQ(result[6], "");
    }
}

TEST(StringOperations, join)
{
    {
        std::vector<std::string> components = {};
        EXPECT_EQ(
            arc::str::join(components.begin(), components.end(), "/"),
            ""
        );
    }
    {
        std::vector<std::string> components = {
            "this",
            "is",
            "",
            "a",
            "test"
        };
        EXPECT_EQ(
            arc::str::join(components.begin(), components.end(), "/"),
            "this/is//a/test"
        );
    }
    {
        std::vector<std::string> components = {
            "",
            "this",
            "is",
            "a",
            "test",
            ""
        };
        EXPECT_EQ(
            arc::str::join(components.begin(), components.end(), "^&*"),
            "^&*this^&*is^&*a^&*test^&*"
        );
    }
}

TEST(StringOperations, lstrip)
{
    EXPECT_EQ(arc::str::lstrip(""), "");
    EXPECT_EQ(arc::str::lstrip("   "), "");
    EXPECT_EQ(arc::str::lstrip("   hello"), "hello");
    EXPECT_EQ(arc::str::lstrip("   hello", ""), "   hello");
    EXPECT_EQ(arc::str::lstrip(" \n\t hello\t"), "hello\t");
    EXPECT_EQ(arc::str::lstrip("**!!&&hello!!", "&*!"), "hello!!");
}

TEST(StringOperations, rstrip)
{
    EXPECT_EQ(arc::str::rstrip(""), "");
    EXPECT_EQ(arc::str::rstrip("   "), "");
    EXPECT_EQ(arc::str::rstrip("hello      "), "hello");
    EXPECT_EQ(arc::str::rstrip("hello    ", ""), "hello    ");
    EXPECT_EQ(arc::str::rstrip(" \n\t hello\t  \n"), " \n\t hello");
    EXPECT_EQ(arc::str::rstrip("**!!&&hello**!!&&", "&*!"), "**!!&&hello");
}
