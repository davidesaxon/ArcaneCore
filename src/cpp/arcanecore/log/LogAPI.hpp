/*!
 * \file
 * \author David Saxon
 * \brief Defines the API of the log module of ArcaneCore.
 *
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ARCANECORE_LOG_LOGAPI_HPP_
#define ARCANECORE_LOG_LOGAPI_HPP_


/*!
 * \brief The major API version number of the ArcaneCore log module.
 */
#define ARCLOG_API_VERSION_MAJOR 0
/*!
 * \brief The minor API version number of the ArcaneCore log module.
 */
#define ARCLOG_API_VERSION_MINOR 1


#ifndef IN_DOXYGEN

#define ARCLOG_BUILD_VER_NS2(major, minor) v##major##_##minor
#define ARCLOG_BUILD_VER_NS(major, minor) ARCLOG_BUILD_VER_NS2(major, minor)

#define ARCLOG_VERSION_NS         \
    ARCLOG_BUILD_VER_NS(          \
        ARCLOG_API_VERSION_MAJOR, \
        ARCLOG_API_VERSION_MINOR  \
    )

#endif // IN_DOXYGEN

#define ARCLOG_VERSION_NS_BEGIN
#ifndef IN_DOXYGEN
    #undef ARCLOG_VERSION_NS_BEGIN
    #define ARCLOG_VERSION_NS_BEGIN inline namespace ARCLOG_VERSION_NS {
#endif

#define ARCLOG_VERSION_NS_END
#ifndef IN_DOXYGEN
    #undef ARCLOG_VERSION_NS_END
    #define ARCLOG_VERSION_NS_END }
#endif

#endif
