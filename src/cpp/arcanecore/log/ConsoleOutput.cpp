/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <iostream>
#include <string>

#include <arcanecore/base/Preproc.hpp>

#ifdef ARC_OS_WINDOWS
#include <windows.h>
#endif

#include "arcanecore/log/ConsoleOutput.hpp"


namespace arclog
{
ARCLOG_VERSION_NS_BEGIN

//------------------------------------------------------------------------------
//                                  CONSTRUCTOR
//------------------------------------------------------------------------------

ConsoleOutput::ConsoleOutput(arclog::Verbosity verbosity, bool colored)
    : arclog::AbstractOutput(verbosity)
    , m_colored             (colored)
{
}

//------------------------------------------------------------------------------
//                                   DESTRUCTOR
//------------------------------------------------------------------------------

ConsoleOutput::~ConsoleOutput()
{
}

//------------------------------------------------------------------------------
//                            PUBLIC MEMBER FUNCTIONS
//------------------------------------------------------------------------------

bool ConsoleOutput::is_colored() const
{
    return m_colored;
}

void ConsoleOutput::set_colored(bool state)
{
    m_colored = state;
}

//------------------------------------------------------------------------------
//                           PROTECTED MEMBER FUNCTIONS
//------------------------------------------------------------------------------

void ConsoleOutput::write(
        arclog::Verbosity verbosity,
        std::string const& input_name,
        std::string const& message)
{
    // early exit?
    if(static_cast<int>(verbosity) > static_cast<int>(m_verbosity))
    {
        return;
    }

    // format the message
    std::string prefix;
    if(!input_name.empty())
    {
        prefix = "{" + input_name + "} - ";
    }

    switch(verbosity)
    {
        case arclog::Verbosity::kCritical:
        {
            #ifdef ARC_OS_WINDOWS
            HANDLE console_handle = GetStdHandle(STD_ERROR_HANDLE);
            CONSOLE_SCREEN_BUFFER_INFO console_info;
            GetConsoleScreenBufferInfo(console_handle, &console_info);
            SetConsoleTextAttribute(
                console_handle,
                FOREGROUND_RED | FOREGROUND_INTENSITY | COMMON_LVB_UNDERSCORE
            );
            #endif

            std::cerr
            #ifdef ARC_OS_UNIX
                << "\033[04;31m"
            #endif
                << prefix << "[CRITICAL]: " << message
            #ifdef ARC_OS_UNIX
                << "\033[00m"
            #endif
                << std::endl;

            #ifdef ARC_OS_WINDOWS
            SetConsoleTextAttribute(console_handle, console_info.wAttributes);
            #endif

            break;
        }
        case arclog::Verbosity::kError:
        {
            #ifdef ARC_OS_WINDOWS
            HANDLE console_handle = GetStdHandle(STD_ERROR_HANDLE);
            CONSOLE_SCREEN_BUFFER_INFO console_info;
            GetConsoleScreenBufferInfo(console_handle, &console_info);
            SetConsoleTextAttribute(
                console_handle,
                FOREGROUND_RED | FOREGROUND_INTENSITY
            );
            #endif

            std::cerr
            #ifdef ARC_OS_UNIX
                << "\033[00;31m"
            #endif
                << prefix << "[ERROR]: " << message
            #ifdef ARC_OS_UNIX
                << "\033[00m"
            #endif
                << std::endl;

            #ifdef ARC_OS_WINDOWS
            SetConsoleTextAttribute(console_handle, console_info.wAttributes);
            #endif

            break;
        }
        case arclog::Verbosity::kWarning:
        {
            #ifdef ARC_OS_WINDOWS
            HANDLE console_handle = GetStdHandle(STD_ERROR_HANDLE);
            CONSOLE_SCREEN_BUFFER_INFO console_info;
            GetConsoleScreenBufferInfo(console_handle, &console_info);
            SetConsoleTextAttribute(
                console_handle,
                FOREGROUND_GREEN | FOREGROUND_RED | FOREGROUND_INTENSITY
            );
            #endif

            std::cerr
            #ifdef ARC_OS_UNIX
                << "\033[00;33m"
            #endif
                << prefix << "[ERROR]: " << message
            #ifdef ARC_OS_UNIX
                << "\033[00m"
            #endif
                << std::endl;

            #ifdef ARC_OS_WINDOWS
            SetConsoleTextAttribute(console_handle, console_info.wAttributes);
            #endif

            break;
        }
        case arclog::Verbosity::kInfo:
        {
            std::cout << prefix << "[INFO]: " << message << std::endl;
            break;
        }
        default:
        {
            #ifdef ARC_OS_WINDOWS
            HANDLE console_handle = GetStdHandle(STD_OUTPUT_HANDLE);
            CONSOLE_SCREEN_BUFFER_INFO console_info;
            GetConsoleScreenBufferInfo(console_handle, &console_info);
            SetConsoleTextAttribute(
                console_handle,
                FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_INTENSITY
            );
            #endif

            std::cout
            #ifdef ARC_OS_UNIX
                << "\033[00;36m"
            #endif
                << prefix << "[DEBUG]: " << message
            #ifdef ARC_OS_UNIX
                << "\033[00m"
            #endif
                << std::endl;

            #ifdef ARC_OS_WINDOWS
            SetConsoleTextAttribute(console_handle, console_info.wAttributes);
            #endif

            break;
        }
    }
}

ARCLOG_VERSION_NS_END
} // namespace arclog
