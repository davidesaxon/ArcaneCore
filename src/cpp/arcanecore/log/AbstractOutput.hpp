/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ARCANECORE_LOG_ABSTRACTOUTPUT_HPP_
#define ARCANECORE_LOG_ABSTRACTOUTPUT_HPP_

#include <unordered_set>

#include "arcanecore/log/Hub.hpp"
#include "arcanecore/log/LogAPI.hpp"
#include "arcanecore/log/Verbosity.hpp"


namespace arclog
{
ARCLOG_VERSION_NS_BEGIN

/*!
 * \brief Object that writes logging messages an arbitrary output (e.g. console
 *        or file).
 *
 * Inputs are connected to an output via an arclog::Hub.
 */
class AbstractOutput
    : private arc::lang::Noncopyable
    , private arc::lang::Nonmovable
    , private arc::lang::Noncomparable
{
private:

    //--------------------------------------------------------------------------
    //                                  FRIENDS
    //--------------------------------------------------------------------------

    friend class Hub;

public:

    //--------------------------------------------------------------------------
    //                                CONSTRUCTOR
    //--------------------------------------------------------------------------

    /*!
     * \brief Creates a new logging output that is not initially attached to
     *        any Hubs.
     *
     * \param verbosity The initial verbosity level of this output.
     */
    AbstractOutput(arclog::Verbosity verbosity = arclog::Verbosity::kInfo);

    //--------------------------------------------------------------------------
    //                                 DESTRUCTOR
    //--------------------------------------------------------------------------

    ~AbstractOutput();

    //--------------------------------------------------------------------------
    //                          PUBLIC MEMBER FUNCTIONS
    //--------------------------------------------------------------------------

    /*!
     * \brief Returns whether this logging input is attached to at least one
     *        hub.
     */
    bool is_attached() const;

    /*!
     * \brief Returns the verbosity level of this logging output.
     */
    arclog::Verbosity get_verbosity() const;

    /*!
     * \brief Sets the verbosity level of this logging output.
     */
    void set_verbosity(arclog::Verbosity verbosity);

protected:

    //--------------------------------------------------------------------------
    //                            PROTECTED ATTRIBUTES
    //--------------------------------------------------------------------------

    /*!
     * \brief The current verbosity level of this logging output.
     */
    arclog::Verbosity m_verbosity;

    //--------------------------------------------------------------------------
    //                         PROTECTED MEMBER FUNCTIONS
    //--------------------------------------------------------------------------

    /*!
     * \brief Asks this output to write the message at the given verbosity level
     *        for an input.
     */
    virtual void write(
            arclog::Verbosity verbosity,
            std::string const& input_name,
            std::string const& message) = 0;

private:

    //--------------------------------------------------------------------------
    //                             PRIVATE ATTRIBUTES
    //--------------------------------------------------------------------------

    // the logging hubs this output is connected to
    std::unordered_set<arclog::Hub*> m_hubs;
};

ARCLOG_VERSION_NS_END
} // namespace arclog

#endif
