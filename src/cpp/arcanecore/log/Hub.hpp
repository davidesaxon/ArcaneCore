/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ARCANECORE_LOG_HUB_HPP_
#define ARCANECORE_LOG_HUB_HPP_

#include <unordered_set>

#include <arcanecore/base/lang/Restrictors.hpp>

#include "arcanecore/log/LogAPI.hpp"
#include "arcanecore/log/Verbosity.hpp"


namespace arclog
{
ARCLOG_VERSION_NS_BEGIN

//------------------------------------------------------------------------------
//                              FORWARD DECLARATIONS
//------------------------------------------------------------------------------

class AbstractOutput;
class Input;


/*!
 * \brief A logging hub is used to connect 0 or more logging inputs to 0 or more
 *        logging outputs.
 *
 * All inputs in a hub are connected to all outputs in the same hub. Inputs and
 * outputs however may be attached to multiple hubs.
 */
class Hub
    : private arc::lang::Noncopyable
    , private arc::lang::Nonmovable
    , private arc::lang::Noncomparable
{
public:

    //--------------------------------------------------------------------------
    //                                CONSTRUCTOR
    //--------------------------------------------------------------------------

    /*!
     * \brief Creates a new logging hub with no inputs or outputs attached yet.
     */
    Hub();

    //--------------------------------------------------------------------------
    //                                 DESTRUCTOR
    //--------------------------------------------------------------------------

    ~Hub();

    //--------------------------------------------------------------------------
    //                          PUBLIC MEMBER FUNCTIONS
    //--------------------------------------------------------------------------

    /*!
     * \brief Attaches the logging input to this hub.
     */
    void attach(arclog::Input* input);

    /*!
     * \brief Attaches the logging output to this hub.
     */
    void attach(arclog::AbstractOutput* output);

    /*!
     * \brief Detaches the given input from this hub (if it is attached).
     */
    void detach(arclog::Input* input);

    /*!
     * \brief Detaches the given output from this hub (if it is attached).
     */
    void detach(arclog::AbstractOutput* output);

    /*!
     * \brief Detaches all inputs from this hub.
     */
    void detach_all_inputs();

    /*!
     * \brief Detaches all outputs from this hub.
     */
    void detach_all_outputs();

    /*!
     * \brief Detaches all inputs and outputs from this hub.
     */
    void detach_all();

#ifndef IN_DOXYGEN

    // sends a message from an input to all outputs
    void send(
            arclog::Verbosity verbosity,
            std::string const& input_name,
            std::string const& message);

#endif // IN_DOXYGEN

private:

    //--------------------------------------------------------------------------
    //                             PRIVATE ATTRIBUTES
    //--------------------------------------------------------------------------

    // the inputs attached to this hub
    std::unordered_set<arclog::Input*> m_inputs;
    // the outputs attached to this hub
    std::unordered_set<arclog::AbstractOutput*> m_outputs;
};

ARCLOG_VERSION_NS_END
} // namespace arclog

#endif
