/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "arcanecore/log/AbstractOutput.hpp"
#include "arcanecore/log/Hub.hpp"
#include "arcanecore/log/Input.hpp"


namespace arclog
{
ARCLOG_VERSION_NS_BEGIN

//------------------------------------------------------------------------------
//                                  CONSTRUCTOR
//------------------------------------------------------------------------------

Hub::Hub()
{
}

//------------------------------------------------------------------------------
//                                   DESTRUCTOR
//------------------------------------------------------------------------------

Hub::~Hub()
{
}

//------------------------------------------------------------------------------
//                            PUBLIC MEMBER FUNCTIONS
//------------------------------------------------------------------------------

void Hub::attach(arclog::Input* input)
{
    // already attached?
    if(m_inputs.find(input) != m_inputs.end())
    {
        return;
    }

    m_inputs.insert(input);
    input->m_hubs.insert(this);
}

void Hub::attach(arclog::AbstractOutput* output)
{
    // already attached?
    if(m_outputs.find(output) != m_outputs.end())
    {
        return;
    }

    m_outputs.insert(output);
    output->m_hubs.insert(this);
}

void Hub::detach(arclog::Input* input)
{
    auto f_input = m_inputs.find(input);
    if(f_input != m_inputs.end())
    {
        auto f_hub = input->m_hubs.find(this);
        if(f_hub != input->m_hubs.end())
        {
            input->m_hubs.erase(f_hub);
        }
        m_inputs.erase(f_input);
    }
}

void Hub::detach(arclog::AbstractOutput* output)
{
    auto f_output = m_outputs.find(output);
    if(f_output != m_outputs.end())
    {
        auto f_hub = output->m_hubs.find(this);
        if(f_hub != output->m_hubs.end())
        {
            output->m_hubs.erase(f_hub);
        }
        m_outputs.erase(f_output);
    }
}

void Hub::detach_all_inputs()
{
    for(arclog::Input* input : m_inputs)
    {
        detach(input);
    }
}

void Hub::detach_all_outputs()
{
    for(arclog::AbstractOutput* output : m_outputs)
    {
        detach(output);
    }
}

void Hub::detach_all()
{
    detach_all_inputs();
    detach_all_outputs();
}

void Hub::send(
        arclog::Verbosity verbosity,
        std::string const& input_name,
        std::string const& message)
{
    for(arclog::AbstractOutput* output : m_outputs)
    {
        output->write(verbosity, input_name, message);
    }
}

ARCLOG_VERSION_NS_END
} // namespace arclog
