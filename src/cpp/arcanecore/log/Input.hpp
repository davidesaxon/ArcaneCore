/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ARCANECORE_LOG_INPUT_HPP_
#define ARCANECORE_LOG_INPUT_HPP_

#include <unordered_set>

#include <arcanecore/base/lang/Restrictors.hpp>

#include <fmt/format.h>

#include "arcanecore/log/Hub.hpp"
#include "arcanecore/log/LogAPI.hpp"
#include "arcanecore/log/Verbosity.hpp"


namespace arclog
{
ARCLOG_VERSION_NS_BEGIN

/*!
 * \brief Object used to write logging messages to arbitrary outputs.
 *
 * This input is connected to outputs via an arclog::Hub.
 */
class Input
    : private arc::lang::Noncopyable
    , private arc::lang::Nonmovable
    , private arc::lang::Noncomparable
{
private:

    //--------------------------------------------------------------------------
    //                                  FRIENDS
    //--------------------------------------------------------------------------

    friend class Hub;

public:

    //--------------------------------------------------------------------------
    //                                CONSTRUCTOR
    //--------------------------------------------------------------------------

    /*!
     * \brief Creates a new a logging input with the given name.
     */
    Input(std::string const& name);

    //--------------------------------------------------------------------------
    //                                 DESTRUCTOR
    //--------------------------------------------------------------------------

    ~Input();

    //--------------------------------------------------------------------------
    //                          PUBLIC MEMBER FUNCTIONS
    //--------------------------------------------------------------------------

    /*!
     * \brief Returns whether this logging input is attached to at least one
     *        hub.
     */
    bool is_attached() const;

    /*!
     * \brief Returns the name of this logging input.
     */
    std::string const& get_name() const;

    /*!
     * \brief Writes a message at critical verbosity level using the fmt calling
     *        convention.
     */
    template<typename ... Args>
    void critical(char const* format, Args const& ... args)
    {
        // no hubs?
        if(m_hubs.empty())
        {
            return;
        }

        std::string message =
            fmt::vformat(format, fmt::make_format_args(args...));
        for(arclog::Hub* hub : m_hubs)
        {
            hub->send(arclog::Verbosity::kCritical, m_name, message);
        }
    }

    /*!
     * \brief Writes a message at error verbosity level using the fmt calling
     *        convention.
     */
    template<typename ... Args>
    void error(char const* format, Args const& ... args)
    {
        // no hubs?
        if(m_hubs.empty())
        {
            return;
        }

        std::string message =
            fmt::vformat(format, fmt::make_format_args(args...));
        for(arclog::Hub* hub : m_hubs)
        {
            hub->send(arclog::Verbosity::kError, m_name, message);
        }
    }

    /*!
     * \brief Writes a message at warning verbosity level using the fmt calling
     *        convention.
     */
    template<typename ... Args>
    void warning(char const* format, Args const& ... args)
    {
        // no hubs?
        if(m_hubs.empty())
        {
            return;
        }

        std::string message =
            fmt::vformat(format, fmt::make_format_args(args...));
        for(arclog::Hub* hub : m_hubs)
        {
            hub->send(arclog::Verbosity::kWarning, m_name, message);
        }
    }

    /*!
     * \brief Writes a message at info verbosity level using the fmt calling
     *        convention.
     */
    template<typename ... Args>
    void info(char const* format, Args const& ... args)
    {
        // no hubs?
        if(m_hubs.empty())
        {
            return;
        }

        std::string message =
            fmt::vformat(format, fmt::make_format_args(args...));
        for(arclog::Hub* hub : m_hubs)
        {
            hub->send(arclog::Verbosity::kInfo, m_name, message);
        }
    }

    /*!
     * \brief Writes a message at debug verbosity level using the fmt calling
     *        convention.
     */
    template<typename ... Args>
    void debug(char const* format, Args const& ... args)
    {
        // no hubs?
        if(m_hubs.empty())
        {
            return;
        }

        std::string message =
            fmt::vformat(format, fmt::make_format_args(args...));
        for(arclog::Hub* hub : m_hubs)
        {
            hub->send(arclog::Verbosity::kDebug, m_name, message);
        }
    }

private:

    //--------------------------------------------------------------------------
    //                             PRIVATE ATTRIBUTES
    //--------------------------------------------------------------------------

    // the name of this logging input
    std::string const m_name;
    // the logging hubs this input is connected to
    std::unordered_set<arclog::Hub*> m_hubs;
};

ARCLOG_VERSION_NS_END
} // namespace arclog

#endif
