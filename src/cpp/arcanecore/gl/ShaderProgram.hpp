/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ARCANECORE_GL_SHADERPROGRAM_HPP_
#define ARCANECORE_GL_SHADERPROGRAM_HPP_

#include <string>

#include <GL/glew.h>

#include <arcanecore/base/fsys/Path.hpp>
#include <arcanecore/base/lang/Restrictors.hpp>
#include <arcanecore/cxx17/string_view.hpp>
#include <arcanecore/lx/Matrix.hpp>
#include <arcanecore/lx/Vector.hpp>

#include "arcanecore/gl/GLAPI.hpp"


namespace arcgl
{
ARCGL_VERSION_NS_BEGIN

/*!
 * \brief Holds a reference counted instance of an OpenGL shader program.
 *
 * Copies of this object will all point to the same shader program and the
 * program will not be released from memory until all reference counts are
 * destroyed or the release member function is explicitly called.
 */
class ShaderProgram
    : private arc::lang::Noncomparable
{
public:

    //--------------------------------------------------------------------------
    //                                CONSTRUCTORS
    //--------------------------------------------------------------------------

    /*!
     * \brief Creates a new OpenGL shader program with no shaders yet attached.
     */
    ShaderProgram();

    /*!
     * \brief Takes a new reference count of the given shader program.
     */
    ShaderProgram(ShaderProgram const& other);

    /*!
     * \brief Move constructor.
     */
    ShaderProgram(ShaderProgram&& other);

    //--------------------------------------------------------------------------
    //                                 DESTRUCTOR
    //--------------------------------------------------------------------------

    ~ShaderProgram();

    //--------------------------------------------------------------------------
    //                                 OPERATORS
    //--------------------------------------------------------------------------

    /*!
     * \brief Decrements the reference count of the this object's current shader
     *        program and takes a new reference count of the given shader
     *        program.
     */
    ShaderProgram& operator=(ShaderProgram const& other);

    /*!
     * \brief Move assignment operator.
     */
    ShaderProgram& operator=(ShaderProgram&& other);

    //--------------------------------------------------------------------------
    //                          PUBLIC STATIC FUNCTIONS
    //--------------------------------------------------------------------------

    /*!
     * \brief Converts the given GL shader type to a string representation.
     */
    static std::string shader_type_to_string(GLenum shader_type);

    //--------------------------------------------------------------------------
    //                          PUBLIC MEMBER FUNCTIONS
    //--------------------------------------------------------------------------

    /*!
     * \brief Attaches and compiles the shader source within the given string.
     *
     * \note Requires link() to be called to actually generate a program with
     *       this shader.
     *
     * \param shader_type Specifies the type of shader to be created.
     * \param source The string containing the source code of the shader.
     *
     * \throw arc::ex::ParseError If the shader could not be parsed as a glsl
     *                            shader.
     */
    void attach_shader(
            GLenum shader_type,
            std::string_view const& source);

    void set_attribute_location(GLuint index, std::string const& location);

    /*!
     * \brief Attaches and compiles the shader source within the given file.
     *
     * \note Requires link() to be called to actually generate a program with
     *       this shader.
     *
     * \param shader_type Specifies the type of shader to be created.
     * \param source_path The file path containing the source code of the
     *                    shader.
     *
     * \throw arc::ex::ParseError If the shader could not be parsed as a glsl
     *                            shader.
     */
    void attach_shader(
            GLenum shader_type,
            arc::fsys::Path const& source_path);

    /*!
     * \brief Links the currently attached and compiled shaders into a single
     *        program.
     *
     * \throw arc::ex::GraphicsError If problem occurs while linking the
     *                               shaders.
     */
    void link();

    /*!
     * \brief Releases the current shader program (if was successfully linked).
     */
    void release();

    /*!
     * \brief Binds this as the current shader program.
     */
    void bind();

    /*!
     * \brief Unbinds the current shader program.
     */
    void unbind();

    /*!
     * \brief Returns the native OpenGL id of the shader program.
     */
    GLuint get_native() const;

    /*!
     * \brief Returns the location of an uniform variable within this shader
     *        program or -1 if the location does not exist.
     */
    GLint get_uniform_location(char const* location);

    /*!
     * \brief Passes the given signed int as an uniform to this shader program.
     *
     * \param location The location of the uniform to set.
     * \param value The value to set at the uniform location.
     */
    void set_uniform_1i(char const* location, int32_t value);

    /*!
     * \brief Passes the given unsigned int as an uniform to this shader
     *        program.
     *
     * \param location The location of the uniform to set.
     * \param value The value to set at the uniform location.
     */
    void set_uniform_1u(char const* location, uint32_t value);

    /*!
     * \brief Passes the given float as an uniform to this shader program.
     *
     * \param location The location of the uniform to set.
     * \param value The value to set at the uniform location.
     */
    void set_uniform_1f(char const* location, float value);

    /*!
     * \brief Passes the given Vector3f as an uniform to this shader program.
     *
     * \param location The location of the uniform to set.
     * \param value The value to set at the uniform location.
     */
    void set_uniform_3f(char const* location, arclx::Vector3f const& value);

    /*!
     * \brief Passes the given Matrix44f as an uniform to this shader program.
     *
     * \param location The location of the uniform to set.
     * \param value The value to set at the uniform location.
     */
    void set_uniform_44f(char const* location, arclx::Matrix44f const& value);

private:

    //--------------------------------------------------------------------------
    //                                    DATA
    //--------------------------------------------------------------------------

    class ShaderProgramData;
    ShaderProgramData* m_data;

    //--------------------------------------------------------------------------
    //                          PRIVATE MEMBER FUNCTIONS
    //--------------------------------------------------------------------------

    // increases the reference count to this object's internal data.
    void increase_reference();

    // decreases the reference count to this object's internal data, if this
    // holds the last reference to the data, the data will be deleted.
    void decrease_reference();
};

ARCGL_VERSION_NS_END
} // namespace arcgl

#endif
