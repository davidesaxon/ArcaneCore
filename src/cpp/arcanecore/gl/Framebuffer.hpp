/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ARCANECORE_GL_FRAMEBUFFER_HPP_
#define ARCANECORE_GL_FRAMEBUFFER_HPP_

#include <GL/glew.h>

#include <arcanecore/base/lang/Restrictors.hpp>
#include <arcanecore/lx/Vector.hpp>

#include "arcanecore/gl/GLAPI.hpp"


namespace arcgl
{
ARCGL_VERSION_NS_BEGIN

//------------------------------------------------------------------------------
//                              FORWARD DECLARATIONS
//------------------------------------------------------------------------------

class Texture2D;

/*!
 * \brief Holds a reference counted instance of an OpenGL framebuffer.
 *
 * Copies of this object will all point to the same framebuffer and the
 * framebuffer will not be released from memory until all reference counts are
 * destroyed or the release member function is explicitly called.
 */
class Framebuffer
    : private arc::lang::Noncomparable
{
public:

    //--------------------------------------------------------------------------
    //                                CONSTRUCTORS
    //--------------------------------------------------------------------------

    /*!
     * \brief Creates a new uninitialized framebuffer.
     */
    Framebuffer();

    /*!
     * \brief Takes a reference count of the given framebuffer.
     */
    Framebuffer(Framebuffer const& other);

    /*!
     * \brief Move constructor.
     */
    Framebuffer(Framebuffer&& other);

    //--------------------------------------------------------------------------
    //                                 DESTRUCTOR
    //--------------------------------------------------------------------------

    ~Framebuffer();

    //--------------------------------------------------------------------------
    //                                 OPERATORS
    //--------------------------------------------------------------------------

    /*!
     * \brief Decrements the reference count of the this object's current
     *        framebuffer and takes a new reference count of the given
     *        framebuffer.
     */
    Framebuffer& operator=(Framebuffer const& other);

    /*!
     * \brief Move assignment operator.
     */
    Framebuffer& operator=(Framebuffer&& other);

    //--------------------------------------------------------------------------
    //                          PUBLIC MEMBER FUNCTIONS
    //--------------------------------------------------------------------------

    /*!
     * \brief Initializes this framebuffer at the given resolution.
     */
    void init(arclx::Vector2u const& resolution);

    /*!
     * \brief Releases the current data of this framebuffer.
     */
    void release();

    /*!
     * \brief Attaches the given Texture2D as this Framebuffer's color
     *        component.
     */
    void attach_color_texture(Texture2D const& texture);

    /*!
     * \brief Attaches the given Texture2D as this Framebuffer's depth
     *        component.
     */
    void attach_depth_texture(Texture2D const& texture);

    /*!
     * \brief Makes this the current bound framebuffer.
     */
    void bind();

    /*!
     * \brief Unbinds the current framebuffer.
     */
    void unbind();

    /*!
     * \brief Returns the native OpenGL id of the framebuffer.
     */
    GLuint get_native() const;

private:

    //--------------------------------------------------------------------------
    //                                    DATA
    //--------------------------------------------------------------------------

    class FramebufferData;
    FramebufferData* m_data;

    //--------------------------------------------------------------------------
    //                          PRIVATE MEMBER FUNCTIONS
    //--------------------------------------------------------------------------

    // increases the reference count to this object's internal data.
    void increase_reference();

    // decreases the reference count to this object's internal data, if this
    // holds the last reference to the data, the data will be deleted.
    void decrease_reference();
};

ARCGL_VERSION_NS_END
} // namespace arcgl

#endif
