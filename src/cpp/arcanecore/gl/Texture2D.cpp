/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "arcanecore/gl/Texture2D.hpp"


namespace arcgl
{
ARCGL_VERSION_NS_BEGIN

//------------------------------------------------------------------------------
//                                      DATA
//------------------------------------------------------------------------------

class Texture2D::TextureData
    : private arc::lang::Noncopyable
    , private arc::lang::Nonmovable
    , private arc::lang::Noncomparable
{
public:

    // the number of references pointing to this data instance
    std::size_t m_ref_count;

private:

    //-------------------P R I V A T E    A T T R I B U T E S-------------------

    // the id of the texture
    GLuint m_id;
    // filtering modes
    GLenum m_min_filter;
    GLenum m_mag_filter;
    // wrap modes
    GLenum m_wrap_s;
    GLenum m_wrap_t;

public:

    //--------------------------C O N S T R U C T O R---------------------------

    TextureData()
        : m_ref_count (1)
        , m_id        (0)
        , m_min_filter(GL_NEAREST)
        , m_mag_filter(GL_NEAREST)
        , m_wrap_s    (GL_CLAMP)
        , m_wrap_t    (GL_CLAMP)
    {
    }

    //---------------------------D E S T R U C T O R----------------------------

    ~TextureData()
    {
    }

    //-------------P U B L I C    M E M B E R    F U N C T I O N S--------------

    void init(
            GLint internal_format,
            arclx::Vector2u const& resolution,
            GLenum format,
            GLenum data_type,
            GLvoid const* data)
    {
        release();
        glEnable(GL_TEXTURE_2D);

        // generate
        glGenTextures(1, &m_id);
        bind();
        glTexImage2D(
            GL_TEXTURE_2D,
            0,
            internal_format,
            resolution(0),
            resolution(1),
            0,
            format,
            data_type,
            data
        );
        unbind();

        // apply modes
        apply_filtering();
        apply_wrap();
    }

    void release()
    {
        if(m_id != 0)
        {
            glDeleteTextures(1, &m_id);
            m_id = 0;
        }
    }

    void update(
            arclx::Vector2u const& offset,
            arclx::Vector2u const& size,
            GLenum format,
            GLenum data_type,
            GLvoid const* data)
    {
        bind();
        glTexSubImage2D(
            GL_TEXTURE_2D,
            0,
            offset(0),
            offset(1),
            size(0),
            size(1),
            format,
            data_type,
            data
        );
    }

    void set_filtering(GLenum min_filter, GLenum mag_filter)
    {
        m_min_filter = min_filter;
        m_mag_filter = mag_filter;
        apply_filtering();
    }

    void set_wrap(GLenum wrap_s, GLenum wrap_t)
    {
        m_wrap_s = wrap_s;
        m_wrap_t = wrap_t;
        apply_wrap();
    }

    void bind()
    {
        glBindTexture(GL_TEXTURE_2D, m_id);
    }

    void unbind()
    {
        glBindTexture(GL_TEXTURE_2D, 0);
    }

    GLuint get_native() const
    {
        return m_id;
    }

private:

    //------------P R I V A T E    M E M B E R    F U N C T I O N S-------------

    // applies filtering mode to this texture (if it is setup)
    void apply_filtering()
    {
        if(m_id == 0)
        {
            return;
        }

        glBindTexture(GL_TEXTURE_2D, m_id);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, m_min_filter);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, m_mag_filter);
    }

    // applies wrap mode to this texture (if it is setup)
    void apply_wrap()
    {
        if(m_id == 0)
        {
            return;
        }

        glBindTexture(GL_TEXTURE_2D, m_id);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, m_wrap_s);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, m_wrap_t);
    }
};

//------------------------------------------------------------------------------
//                                  CONSTRUCTORS
//------------------------------------------------------------------------------

Texture2D::Texture2D()
    : m_data(new TextureData())
{
}

Texture2D::Texture2D(Texture2D const& other)
    : m_data(other.m_data)
{
    increase_reference();
}

Texture2D::Texture2D(Texture2D&& other)
    : m_data(other.m_data)
{
    other.m_data = nullptr;
}

//------------------------------------------------------------------------------
//                                   DESTRUCTOR
//------------------------------------------------------------------------------

Texture2D::~Texture2D()
{
    decrease_reference();
}

//------------------------------------------------------------------------------
//                                   OPERATORS
//------------------------------------------------------------------------------

Texture2D& Texture2D::operator=(Texture2D const& other)
{
    if(m_data == other.m_data)
    {
        return *this;
    }

    decrease_reference();
    m_data = other.m_data;
    increase_reference();

    return *this;
}

Texture2D& Texture2D::operator=(Texture2D&& other)
{
    if(m_data == other.m_data)
    {
        return *this;
    }

    decrease_reference();
    m_data = other.m_data;
    other.m_data = nullptr;

    return *this;
}

//------------------------------------------------------------------------------
//                            PUBLIC MEMBER FUNCTIONS
//------------------------------------------------------------------------------

void Texture2D::init(
        GLint internal_format,
        arclx::Vector2u const& resolution,
        GLenum format,
        GLenum data_type,
        GLvoid const* data)
{
    m_data->init(internal_format, resolution, format, data_type, data);
}

void Texture2D::release()
{
    m_data->release();
}

void Texture2D::update(
        arclx::Vector2u const& offset,
        arclx::Vector2u const& size,
        GLenum format,
        GLenum data_type,
        GLvoid const* data)
{
    m_data->update(offset, size, format, data_type, data);
}

void Texture2D::set_filtering(GLenum min_filter, GLenum mag_filter)
{
    m_data->set_filtering(min_filter, mag_filter);
}

void Texture2D::set_wrap(GLenum wrap_s, GLenum wrap_t)
{
    m_data->set_wrap(wrap_s, wrap_t);
}

void Texture2D::bind()
{
    m_data->bind();
}

void Texture2D::unbind()
{
    m_data->unbind();
}

GLuint Texture2D::get_native() const
{
    return m_data->get_native();
}

//------------------------------------------------------------------------------
//                            PRIVATE MEMBER FUNCTIONS
//------------------------------------------------------------------------------

void Texture2D::increase_reference()
{
    if(m_data == nullptr)
    {
        return;
    }
    ++m_data->m_ref_count;
}

void Texture2D::decrease_reference()
{
    if(m_data == nullptr)
    {
        return;
    }
    if(m_data->m_ref_count == 1)
    {
        delete m_data;
    }
    else
    {
        --m_data->m_ref_count;
    }
}

ARCGL_VERSION_NS_END
} // namespace arcgl
