/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ARCANECORE_GL_TEXTURE2D_HPP_
#define ARCANECORE_GL_TEXTURE2D_HPP_

#include <GL/glew.h>

#include <arcanecore/base/lang/Restrictors.hpp>
#include <arcanecore/lx/Vector.hpp>

#include "arcanecore/gl/GLAPI.hpp"


namespace arcgl
{
ARCGL_VERSION_NS_BEGIN

/*!
 * \brief Holds a reference counted instance of an OpenGL 2D texture.
 *
 * Copies of this object will all point to the same texture and the texture will
 * not be released from memory until all reference counts are destroyed or the
 * release member function is explicitly called.
 */
class Texture2D
    : private arc::lang::Noncomparable
{
public:

    //--------------------------------------------------------------------------
    //                                CONSTRUCTORS
    //--------------------------------------------------------------------------

    /*!
     * \brief Creates a new uninitialized 2D texture.
     */
    Texture2D();

    /*!
     * \brief Takes a new reference count of the given texture.
     */
    Texture2D(Texture2D const& other);

    /*!
     * \brief Move constructor.
     */
    Texture2D(Texture2D&& other);

    //--------------------------------------------------------------------------
    //                                 DESTRUCTOR
    //--------------------------------------------------------------------------

    ~Texture2D();

    //--------------------------------------------------------------------------
    //                                 OPERATORS
    //--------------------------------------------------------------------------

    /*!
     * \brief Decrements the reference count of the this object's current
     *        texture and takes a new reference count of the given texture.
     */
    Texture2D& operator=(Texture2D const& other);

    /*!
     * \brief Move assignment operator.
     */
    Texture2D& operator=(Texture2D&& other);

    //--------------------------------------------------------------------------
    //                          PUBLIC MEMBER FUNCTIONS
    //--------------------------------------------------------------------------

    /*!
     * \brief Initialises the texture with the given data (can be null).
     *
     * \param internal The format of the OpenGL texture that will be created.
     * \param resolution The resolution of the texture (in texels).
     * \param format The format of the input data (e.g. GL_RGBA).
     * \param data_type The type that the input type is expressed in (e.g.
     *                  GL_UNSIGNED_BYTE).
     * \param The input data.
     */
    void init(
            GLint internal_format,
            arclx::Vector2u const& resolution,
            GLenum format,
            GLenum data_type,
            GLvoid const* data);

    /*!
     * \brief Releases the current data of this texture.
     */
    void release();

    /*!
     * \brief Updates a region of the texture with new data.
     *
     * \param offset The offset (in texels) of the region of the texture to
     *               update.
     * \param size The width and height (in texels) or the region of the texture
     *              to update.
     * \param format The format of the input data (e.g. GL_RGBA).
     * \param data_type The type that the input type is expressed in (e.g.
     *                  GL_UNSIGNED_BYTE).
     * \param The input data.
     */
    void update(
            arclx::Vector2u const& offset,
            arclx::Vector2u const& size,
            GLenum format,
            GLenum data_type,
            GLvoid const* data);

    /*!
     * \brief Sets the minimize and magnify filtering that will be used by this
     *        texture.
     *
     * \note This function can be called before or after the init() function.
     */
    void set_filtering(GLenum min_filter, GLenum mag_filter);

    /*!
     * \brief Sets the the s and t texture wrapping modes.
     *
     * \note This function can be called before or after the init() function.
     */
    void set_wrap(GLenum wrap_s, GLenum wrap_t);

    /*!
     * \brief Makes this the current bound texture.
     */
    void bind();

    /*!
     * \brief Unbinds the current texture.
     */
    void unbind();

    /*!
     * \brief Returns the native OpenGL id of the texture.
     */
    GLuint get_native() const;

private:

    //--------------------------------------------------------------------------
    //                                    DATA
    //--------------------------------------------------------------------------

    class TextureData;
    TextureData* m_data;

    //--------------------------------------------------------------------------
    //                          PRIVATE MEMBER FUNCTIONS
    //--------------------------------------------------------------------------

    // increases the reference count to this object's internal data.
    void increase_reference();

    // decreases the reference count to this object's internal data, if this
    // holds the last reference to the data, the data will be deleted.
    void decrease_reference();
};

ARCGL_VERSION_NS_END
} // namespace arcgl

#endif
