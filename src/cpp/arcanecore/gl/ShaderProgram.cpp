/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <fstream>
#include <list>
#include <vector>
#include <unordered_map>

#include <arcanecore/base/Exceptions.hpp>

#include "arcanecore/gl/ShaderProgram.hpp"


namespace arcgl
{
ARCGL_VERSION_NS_BEGIN

//------------------------------------------------------------------------------
//                                      DATA
//------------------------------------------------------------------------------

class ShaderProgram::ShaderProgramData
    : private arc::lang::Noncopyable
    , private arc::lang::Nonmovable
    , private arc::lang::Noncomparable
{
public:

    // the number of references pointing to this data instance
    std::size_t m_ref_count;

private:

    //-------------------P R I V A T E    A T T R I B U T E S-------------------

    // the id of the shader program
    GLuint m_program_id;

    // the currently compiled shaders to link
    std::list<GLuint> m_compiled_shaders;

    // attribute locations
    std::unordered_map<GLuint, std::string> m_attribute_locations;

public:

    //--------------------------C O N S T R U C T O R---------------------------

    ShaderProgramData()
        : m_ref_count (1)
        , m_program_id(0)
    {
    }

    //---------------------------D E S T R U C T O R----------------------------

    ~ShaderProgramData()
    {
    }

    //-------------P U B L I C    M E M B E R    F U N C T I O N S--------------

    void attach_shader(
            GLenum shader_type,
            std::string_view const& source)
    {
        GLuint shader_id = glCreateShader(shader_type);

        // compile the shader
        char const* source_data = source.data();
        GLint const  source_length = static_cast<GLint>(source.length());
        glShaderSource(shader_id, 1, &source_data, &source_length);
        glCompileShader(shader_id);

        // check for errors
        GLint compile_result = GL_FALSE;
        GLint error_length = 0;
        glGetShaderiv(shader_id, GL_COMPILE_STATUS, &compile_result);
        glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &error_length);
        if(error_length > 0)
        {
            std::vector<char> gl_error_message(error_length + 1);
            glGetShaderInfoLog(
                shader_id,
                error_length,
                nullptr,
                &gl_error_message[0]
            );

            throw arc::ex::ParseError(
                "Failed to compile " +
                ShaderProgram::shader_type_to_string(shader_type) +
                "shader with error: " +
                std::string(&gl_error_message[0], error_length)
            );
        }

        m_compiled_shaders.push_back(shader_id);
    }

    void attach_shader(
            GLenum shader_type,
            arc::fsys::Path const& source_path)
    {
        // TODO: doesn't support unicode paths on Windows?

        std::ifstream stream(source_path.to_native().c_str());
        std::stringstream buffer;
        buffer << stream.rdbuf();

        attach_shader(shader_type, buffer.str());
    }

    void set_attribute_location(GLuint index, std::string const& location)
    {
        m_attribute_locations[index] = location;
    }

    void link()
    {
        release();

        m_program_id = glCreateProgram();
        // attach shaders
        for(GLuint shader_id : m_compiled_shaders)
        {
            glAttachShader(m_program_id, shader_id);
        }

        // bind attribute locations
        for(auto const& attribute_location : m_attribute_locations)
        {
            glBindAttribLocation(
                m_program_id,
                attribute_location.first,
                attribute_location.second.c_str()
            );
        }

        // link
        glLinkProgram(m_program_id);

        // check for errors
        GLint link_result = GL_FALSE;
        GLint error_length = 0;
        glGetProgramiv(m_program_id, GL_LINK_STATUS, &link_result);
        glGetProgramiv(m_program_id, GL_INFO_LOG_LENGTH, &error_length);
        if(error_length > 0)
        {
            std::vector<char> gl_error_message(error_length + 1);
            glGetProgramInfoLog(
                m_program_id,
                error_length,
                nullptr,
                &gl_error_message[0]
            );

            throw arc::ex::GraphicsError(
                "Failed to link shader program with error: " +
                std::string(&gl_error_message[0], error_length)
            );
        }
    }

    void release()
    {
        if(m_program_id != 0)
        {
            glDeleteProgram(m_program_id);
            m_program_id = 0;
        }
    }

    void bind()
    {
        glUseProgram(m_program_id);
    }

    void unbind()
    {
        glUseProgram(0);
    }

    GLuint get_native() const
    {
        return m_program_id;
    }

    GLint get_uniform_location(char const* location)
    {
        return glGetUniformLocation(m_program_id, location);
    }

    void set_uniform_1i(char const* location, int32_t value)
    {
        GLint location_id = glGetUniformLocation(m_program_id, location);
        if(location_id >= 0)
        {
            glUniform1i(location_id, value);
        }
    }

    void set_uniform_1u(char const* location, uint32_t value)
    {
        GLint location_id = glGetUniformLocation(m_program_id, location);
        if(location_id >= 0)
        {
            glUniform1ui(location_id, value);
        }
    }

    void set_uniform_1f(char const* location, float value)
    {
        GLint location_id = glGetUniformLocation(m_program_id, location);
        if(location_id >= 0)
        {
            glUniform1f(location_id, value);
        }
    }

    void set_uniform_3f(char const* location, arclx::Vector3f const& value)
    {
        GLint location_id = glGetUniformLocation(m_program_id, location);
        if(location_id >= 0)
        {
            glUniform3f(location_id, value(0), value(1), value(2));
        }
    }

    void set_uniform_44f(char const* location, arclx::Matrix44f const& value)
    {
        GLint location_id = glGetUniformLocation(m_program_id, location);
        if(location_id >= 0)
        {
            glUniformMatrix4fv(location_id, 1, GL_FALSE, &value(0, 0));
        }
    }
};

//------------------------------------------------------------------------------
//                                  CONSTRUCTORS
//------------------------------------------------------------------------------

ShaderProgram::ShaderProgram()
    : m_data(new ShaderProgramData())
{
}

ShaderProgram::ShaderProgram(ShaderProgram const& other)
    : m_data(other.m_data)
{
    increase_reference();
}

ShaderProgram::ShaderProgram(ShaderProgram&& other)
    : m_data(other.m_data)
{
    other.m_data = nullptr;
}

//------------------------------------------------------------------------------
//                                   DESTRUCTOR
//------------------------------------------------------------------------------

ShaderProgram::~ShaderProgram()
{
    decrease_reference();
}

//------------------------------------------------------------------------------
//                                   OPERATORS
//------------------------------------------------------------------------------

ShaderProgram& ShaderProgram::operator=(ShaderProgram const& other)
{
    if(m_data == other.m_data)
    {
        return *this;
    }

    decrease_reference();
    m_data = other.m_data;
    increase_reference();

    return *this;
}

ShaderProgram& ShaderProgram::operator=(ShaderProgram&& other)
{
    if(m_data == other.m_data)
    {
        return *this;
    }

    decrease_reference();
    m_data = other.m_data;
    other.m_data = nullptr;

    return *this;
}

//------------------------------------------------------------------------------
//                            PUBLIC STATIC FUNCTIONS
//------------------------------------------------------------------------------

std::string ShaderProgram::shader_type_to_string(GLenum shader_type)
{
    switch(shader_type)
    {
        case GL_COMPUTE_SHADER:
            return "compute";
        case GL_VERTEX_SHADER:
            return "vertex";
        case GL_TESS_CONTROL_SHADER:
            return "tessellation control";
        case GL_TESS_EVALUATION_SHADER:
            return "tessellation evaluation";
        case GL_GEOMETRY_SHADER:
            return "geometry";
        case GL_FRAGMENT_SHADER:
            return "fragment";
        default:
            return "UNKNOWN";
    }
}

//------------------------------------------------------------------------------
//                            PUBLIC MEMBER FUNCTIONS
//------------------------------------------------------------------------------

void ShaderProgram::attach_shader(
        GLenum shader_type,
        std::string_view const& source)
{
    m_data->attach_shader(shader_type, source);
}

void ShaderProgram::attach_shader(
        GLenum shader_type,
        arc::fsys::Path const& source_path)
{
    m_data->attach_shader(shader_type, source_path);
}

void ShaderProgram::set_attribute_location(
        GLuint index,
        std::string const& location)
{
    m_data->set_attribute_location(index, location);
}

void ShaderProgram::link()
{
    m_data->link();
}

void ShaderProgram::release()
{
    m_data->release();
}

void ShaderProgram::bind()
{
    m_data->bind();
}

void ShaderProgram::unbind()
{
    m_data->unbind();
}

GLuint ShaderProgram::get_native() const
{
    return m_data->get_native();
}

GLint ShaderProgram::get_uniform_location(char const* location)
{
    return m_data->get_uniform_location(location);
}

void ShaderProgram::set_uniform_1i(char const* location, int32_t value)
{
    m_data->set_uniform_1i(location, value);
}

void ShaderProgram::set_uniform_1u(char const* location, uint32_t value)
{
    m_data->set_uniform_1u(location, value);
}

void ShaderProgram::set_uniform_1f(char const* location, float value)
{
    m_data->set_uniform_1f(location, value);
}

void ShaderProgram::set_uniform_3f(
        char const* location,
        arclx::Vector3f const& value)
{
    m_data->set_uniform_3f(location, value);
}

void ShaderProgram::set_uniform_44f(
        char const* location,
        arclx::Matrix44f const& value)
{
    m_data->set_uniform_44f(location, value);
}

//------------------------------------------------------------------------------
//                            PRIVATE MEMBER FUNCTIONS
//------------------------------------------------------------------------------

void ShaderProgram::increase_reference()
{
    if(m_data == nullptr)
    {
        return;
    }
    ++m_data->m_ref_count;
}

void ShaderProgram::decrease_reference()
{
    if(m_data == nullptr)
    {
        return;
    }
    if(m_data->m_ref_count == 1)
    {
        delete m_data;
    }
    else
    {
        --m_data->m_ref_count;
    }
}

ARCGL_VERSION_NS_END
} // namespace arcgl
