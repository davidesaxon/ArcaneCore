/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <arcanecore/lx/Alignment.hpp>

#include "arcanecore/gl/Framebuffer.hpp"
#include "arcanecore/gl/Texture2D.hpp"


namespace arcgl
{
ARCGL_VERSION_NS_BEGIN

//------------------------------------------------------------------------------
//                                      DATA
//------------------------------------------------------------------------------

class Framebuffer::FramebufferData
    : private arc::lang::Noncopyable
    , private arc::lang::Nonmovable
    , private arc::lang::Noncomparable
{
public:

    // the number of references pointing to this data instance
    std::size_t m_ref_count;

private:

    //-------------------P R I V A T E    A T T R I B U T E S-------------------

    // the id of the texture
    GLuint m_id;

    // the current resolution of the framebuffer
    arclx::Vector2u m_resolution;

    // the color component texture
    GLuint m_color_component;
    // the depth component
    GLuint m_depth_component;

public:

    ARCLX_ALIGNED_NEW;

    //--------------------------C O N S T R U C T O R---------------------------

    FramebufferData()
        : m_ref_count      (1)
        , m_id             (0)
        , m_resolution     (0, 0)
        , m_color_component(0)
        , m_depth_component(0)
    {
    }

    //---------------------------D E S T R U C T O R----------------------------

    ~FramebufferData()
    {
    }

    //-------------P U B L I C    M E M B E R    F U N C T I O N S--------------

    void init(arclx::Vector2u const& resolution)
    {
        release();
        m_resolution = resolution;

        // generate
        glGenFramebuffers(1, &m_id);
    }

    void release()
    {
        m_depth_component  = 0;
        m_color_component = 0;
        if(m_id != 0)
        {
            glDeleteFramebuffers(1, &m_id);
            m_id = 0;
        }
    }

    void attach_color_texture(Texture2D const& texture)
    {
        // do nothing if not initialised
        if(m_id == 0)
        {
            return;
        }

        m_color_component = texture.get_native();

        bind();
        glFramebufferTexture2D(
            GL_FRAMEBUFFER,
            GL_COLOR_ATTACHMENT0,
            GL_TEXTURE_2D,
            m_color_component,
            0
        );
    }

    void attach_depth_texture(Texture2D const& texture)
    {
        // do nothing if not initialised
        if(m_id == 0)
        {
            return;
        }

        m_depth_component = texture.get_native();

        bind();
        glFramebufferTexture2D(
            GL_FRAMEBUFFER,
            GL_DEPTH_ATTACHMENT,
            GL_TEXTURE_2D,
            m_depth_component,
            0
        );
    }

    void bind()
    {
        glBindFramebuffer(GL_FRAMEBUFFER, m_id);
    }

    void unbind()
    {
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    GLuint get_native() const
    {
        return m_id;
    }
};

//------------------------------------------------------------------------------
//                                  CONSTRUCTORS
//------------------------------------------------------------------------------

Framebuffer::Framebuffer()
    : m_data(new FramebufferData())
{
}

Framebuffer::Framebuffer(Framebuffer const& other)
    : m_data(other.m_data)
{
    increase_reference();
}

Framebuffer::Framebuffer(Framebuffer&& other)
    : m_data(other.m_data)
{
    other.m_data = nullptr;
}

//------------------------------------------------------------------------------
//                                   DESTRUCTOR
//------------------------------------------------------------------------------

Framebuffer::~Framebuffer()
{
    decrease_reference();
}

//------------------------------------------------------------------------------
//                                   OPERATORS
//------------------------------------------------------------------------------

Framebuffer& Framebuffer::operator=(Framebuffer const& other)
{
    if(m_data == other.m_data)
    {
        return *this;
    }

    decrease_reference();
    m_data = other.m_data;
    increase_reference();

    return *this;
}

Framebuffer& Framebuffer::operator=(Framebuffer&& other)
{
    if(m_data == other.m_data)
    {
        return *this;
    }

    decrease_reference();
    m_data = other.m_data;
    other.m_data = nullptr;

    return *this;
}

//------------------------------------------------------------------------------
//                            PUBLIC MEMBER FUNCTIONS
//------------------------------------------------------------------------------

void Framebuffer::init(arclx::Vector2u const& resolution)
{
    m_data->init(resolution);
}

void Framebuffer::release()
{
    m_data->release();
}

void Framebuffer::attach_color_texture(Texture2D const& texture)
{
    m_data->attach_color_texture(texture);
}

void Framebuffer::attach_depth_texture(Texture2D const& texture)
{
    m_data->attach_depth_texture(texture);
}

void Framebuffer::bind()
{
    m_data->bind();
}

void Framebuffer::unbind()
{
    m_data->unbind();
}

GLuint Framebuffer::get_native() const
{
    return m_data->get_native();
}

//------------------------------------------------------------------------------
//                            PRIVATE MEMBER FUNCTIONS
//------------------------------------------------------------------------------

void Framebuffer::increase_reference()
{
    if(m_data == nullptr)
    {
        return;
    }
    ++m_data->m_ref_count;
}

void Framebuffer::decrease_reference()
{
    if(m_data == nullptr)
    {
        return;
    }
    if(m_data->m_ref_count == 1)
    {
        delete m_data;
    }
    else
    {
        --m_data->m_ref_count;
    }
}

ARCGL_VERSION_NS_END
} // namespace arcgl
