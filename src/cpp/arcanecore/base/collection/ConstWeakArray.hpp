/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ARCANECORE_BASE_COLLECTION_CONSTWEAKARRAY_HPP_
#define ARCANECORE_BASE_COLLECTION_CONSTWEAKARRAY_HPP_

#include "arcanecore/base/BaseAPI.hpp"
#include "arcanecore/base/iterator/ConstRandomAccessIterator.hpp"


namespace arc
{
ARC_BASE_VERSION_NS_BEGIN
namespace collection
{

/*!
 * \brief A stl style container that points to contiguous block of read-only
 *        data that is not owned by this object.
 *
 * This means the validity of this object is use-case specific. The data is only
 * valid until it is deleted by the owner, in which case this object has no way
 * of determining if the data is valid or not.
 *
 * \tparam T_DataType The type of the data this array contains.
 */
template<typename T_DataType>
class ConstWeakArray
{
public:

    //--------------------------------------------------------------------------
    //                              TYPE DEFINITIONS
    //--------------------------------------------------------------------------

    /*!
     * \brief The iterator type of the ConstWeakArray.
     */
    typedef arc::iterator::ConstRandomAccessIterator<T_DataType> iterator;

    //--------------------------------------------------------------------------
    //                                CONSTRUCTOR
    //--------------------------------------------------------------------------

    /*!
     * \brief Constructs a new weak array with no underlying memory.
     */
    ConstWeakArray()
        : m_data(nullptr)
        , m_size(0)
    {
    }

    /*!
     * \brief Constructs a new array that points to the given data.
     *
     * \param data The data this array will hold a weak reference to.
     * \param size The number of elements in the provided data.
     */
    ConstWeakArray(const T_DataType* data, std::size_t size)
        : m_data(data)
        , m_size(size)
    {
    }

    /*!
     * \brief Copy constructor.
     */
    ConstWeakArray(const ConstWeakArray& other)
        : m_data(other.m_data)
        , m_size(other.m_size)
    {
    }

    /*!
     * \brief Move constructor.
     */
    ConstWeakArray(ConstWeakArray&& other)
        : m_data(other.m_data)
        , m_size(other.m_size)
    {
        other.m_data = nullptr;
        other.m_size = 0;
    }

    //--------------------------------------------------------------------------
    //                                 DESTRUCTOR
    //--------------------------------------------------------------------------

    virtual ~ConstWeakArray()
    {
        // no deleting!
    }

    //--------------------------------------------------------------------------
    //                                 OPERATORS
    //--------------------------------------------------------------------------

    /*!
     * \brief Copy assignment operator.
     */
    ConstWeakArray& operator=(const ConstWeakArray& other)
    {
        m_data = other.m_data;
        m_size = other.m_size;

        return *this;
    }

    /*!
     * \brief Move assignment operator.
     */
    ConstWeakArray& operator=(ConstWeakArray&& other)
    {
        m_data = other.m_data;
        m_size = other.m_size;

        other.m_data = nullptr;
        other.m_size = 0;

        return *this;
    }

    /*!
     * \brief Equality operator.
     */
    bool operator==(const ConstWeakArray& other)
    {
        return m_data == other.m_data && m_size == other.m_size;
    }

    /*!
     * \brief Inequality operator.
     */
    bool operator!=(const ConstWeakArray& other)
    {
        return !((*this) == other);
    }

    /*!
     * \brief Returns the element of the array at the given index.
     */
    const T_DataType& operator[](std::size_t index) const
    {
        return at(index);
    }

    //--------------------------------------------------------------------------
    //                          PUBLIC MEMBER FUNCTIONS
    //--------------------------------------------------------------------------

    /*!
     * \brief Returns the number of elements in this array.
     */
    std::size_t size() const
    {
        return m_size;
    }

    /*!
     * \brief Returns whether the vector is empty.
     */
    bool empty() const
    {
        return m_size == 0;
    }

    /*!
     * \brief Returns the element of the array at the given index.
     */
    const T_DataType& at(std::size_t index) const
    {
        return m_data[index];
    }

    /*!
     * \brief Returns the first element in the array.
     */
    const T_DataType& front() const
    {
        return m_data[0];
    }

    /*!
     * \brief Returns the last element in the array.
     */
    const T_DataType& back() const
    {
        return m_data[m_size - 1];
    }

    /*!
     * \brief Returns an iterator to the first element in this array.
     */
    iterator begin() const
    {
        return iterator(m_data);
    }

    /*!
     * \brief Returns an iterator referring to the past-the-end element in this
     *        array.
     */
    iterator end() const
    {
        // note: works for empty data since m_size is 0
        return iterator(m_data + m_size);
    }

private:

    //--------------------------------------------------------------------------
    //                             PRIVATE ATTRIBUTES
    //--------------------------------------------------------------------------

    // the non-owned data of this array
    const T_DataType* m_data;
    // the number of element in this array
    std::size_t m_size;
};

} // namespace collection
ARC_BASE_VERSION_NS_END
} // namespace arc

#endif
