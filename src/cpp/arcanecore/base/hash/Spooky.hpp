/*!
 * \file
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ARCANECORE_BASE_HASH_SPOOKY_HPP_
#define ARCANECORE_BASE_HASH_SPOOKY_HPP_

#include <cstddef>
#include <cstdint>

#include "arcanecore/base/BaseAPI.hpp"


namespace arc
{
ARC_BASE_VERSION_NS_BEGIN
namespace hash
{

//------------------------------------------------------------------------------
//                                   VARIABLES
//------------------------------------------------------------------------------

/*!
 * \brief If set to ```true``` the Spooky hash function will ensure that the
 *        input data is aligned before reading from it.
 *
 * Defaults to ```false```.
 *
 * \warn In most cases this will induce a copy of the data to be made.
 */
extern bool spooky_force_aligned_reads;

//------------------------------------------------------------------------------
//                                   FUNCTIONS
//------------------------------------------------------------------------------

/*!
 * \brief Computes a 128-bit Spooky hash of the given data.
 *
 * \param data The data to hash.
 * \param length The number of bytes in the data.
 * \param out_hash1 Returns the first 64-bits of the hash.
 * \param out_hash2 Returns the second 64-bits of the hash.
 * \param initial1 The first 640-bit initial value to begin the hash with.
 * \param initial2 The second 640-bit initial value to begin the hash with.
 */
void spooky_128(
        void const* data,
        std::size_t length,
        uint64_t& out_hash1,
        uint64_t& out_hash2,
        uint64_t initial1 = 0,
        uint64_t initial2 = 0);

/*!
 * \brief Computes a 64-bit Spooky hash of the given data.
 *
 * \param data The data to hash.
 * \param length The number of bytes in the data.
 * \param initial The initial value to begin the hash with.
 *
 * \return The 64-bit result of the hash.
 */
uint64_t spooky_64(
        void const* data,
        std::size_t length,
        uint64_t initial = 0);

/*!
 * \brief Computes a 32-bit Spooky hash of the given data.
 *
 * \param data The data to hash.
 * \param length The number of bytes in the data.
 * \param initial The initial value to begin the hash with.
 *
 * \return The 32-bit result of the hash.
 */
uint32_t spooky_32(
        void const* data,
        std::size_t length,
        uint32_t initial = 0);

} // namespace hash
ARC_BASE_VERSION_NS_END
} // namespace arc

#endif
