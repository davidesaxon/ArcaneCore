/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "arcanecore/base/hash/FNV.hpp"


namespace arc
{
ARC_BASE_VERSION_NS_BEGIN
namespace hash
{

uint32_t fnv1a_32(void const* data, std::size_t length, uint32_t initial)
{
    unsigned char const* bytes = static_cast<unsigned char const*>(data);
    uint32_t hash = initial;
    for(std::size_t i = 0; i < length; ++i)
    {
        hash = hash ^ bytes[i];
        hash = hash * 0x1000193;
    }
    return hash;
}

uint64_t fnv1a_64(void const* data, std::size_t length, uint64_t initial)
{
    unsigned char const* bytes = static_cast<unsigned char const*>(data);
    uint64_t hash = initial;
    for(std::size_t i = 0; i < length; ++i)
    {
        hash = hash ^ bytes[i];
        hash = hash * 0x100000001B3;
    }
    return hash;
}

} // namespace hash
ARC_BASE_VERSION_NS_END
} // namespace arc
