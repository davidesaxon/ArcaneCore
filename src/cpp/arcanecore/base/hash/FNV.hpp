/*!
 * \file
 * \brief Implementation of the FNV family of hashing functions.
 * \author David Saxon
 *
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ARCANECORE_BASE_HASH_FNV_HPP_
#define ARCANECORE_BASE_HASH_FNV_HPP_

#include <cstddef>
#include <cstdint>

#include "arcanecore/base/BaseAPI.hpp"


namespace arc
{
ARC_BASE_VERSION_NS_BEGIN
namespace hash
{

//------------------------------------------------------------------------------
//                                   FUNCTIONS
//------------------------------------------------------------------------------

/*!
 * \brief Computes a 32-bit FNV-1a hash of the given data.
 *
 * \param data The data to hash.
 * \param length The number of bytes in the data.
 *
 * \return The 32-bit result of the hash.
 */
uint32_t fnv1a_32(
        void const* data,
        std::size_t length,
        uint32_t initial = 0x811C9DC5);

/*!
 * \brief Computes a 64-bit FNV-1a hash of the given data.
 *
 * \param data The data to hash.
 * \param length The number of bytes in the data.
 *
 * \return The 64-bit result of the hash.
 */
uint64_t fnv1a_64(
        void const* data,
        std::size_t length,
        uint64_t initial = 0xCBF29CE484222325);

} // namespace hash
ARC_BASE_VERSION_NS_END
} // namespace arc

#endif
