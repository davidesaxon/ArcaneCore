/*!
 * \file
 * \author David Saxon
 * \brief Defines core preprocessor functionality of ArcaneCore.
 *
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ARCANECORE_BASE_PREPROC_HPP_
#define ARCANECORE_BASE_PREPROC_HPP_


/*!
 * \brief Directive that is defined if the current platform is a Windows
 *        operating system
 */
#define ARC_OS_WINDOWS
// clean up so code can still run as expected
#ifndef _WIN32
    #undef ARC_OS_WINDOWS
#else
    // need to do this if using windows so we can use std::min and std::max
    #define NOMINMAX
#endif

/*!
 * \brief Directive that is defined if the current platform is a Unix based
 *        operating system
 */
#define ARC_OS_UNIX
// clean up so code can still run as expected
#if  !defined(__APPLE__) && !defined(__linux__) && !defined(__unix__)
    #undef ARC_OS_UNIX
#endif

/*!
 * \brief Directive that is defined if the current platform is a Mac
 *        operating system.
 *
 * \note The ARC_OS_UNIX directive will also be defined in this case.
 */
#define ARC_OS_MAC
// clean up so code can still run as expected
#ifndef __APPLE__
    #undef ARC_OS_MAC
#endif

/*!
 * \brief Directive that is defined if the current platform is a Linux
 *        based operating system.
 *
 * \note The ARC_OS_UNIX directive will also be defined in this case.
 */
#define ARC_OS_LINUX
// clean up so code can still run as expected
#ifndef __linux__
    #undef ARC_OS_LINUX
#endif

/*!
 * \brief Directive that is defined if the current could not be detected
 */
#define ARC_OS_UNKNOWN
// clean up so code can still run as expected
#if defined(ARC_OS_WINDOWS) || defined(ARC_OS_UNIX)
    #undef ARC_OS_UNKNOWN
#endif

/*!
 * Uses the name of the given preprocessor as a string literal.
 */
#define ARC_STRINGIFY_NAME(Var) #Var
/*!
 * Uses the value of the given preprocessor as a string literal.
 */
#define ARC_STRINGIFY_VALUE(Var) ARC_STRINGIFY_NAME(Var)

/*!
 * \brief Marco that attempts to force inlining of a function.
 */
#define ARC_FORCE_INLINE inline
#if defined(_MSC_VER)
    #undef ARC_FORCE_INLINE
    #define ARC_FORCE_INLINE __forceinline
#elif defined(__clang__) || defined(__GNUC__) || defined(__INTEL_COMPILER)
    #undef ARC_FORCE_INLINE
    #define ARC_FORCE_INLINE __attribute__((always_inline))
#endif

#endif
