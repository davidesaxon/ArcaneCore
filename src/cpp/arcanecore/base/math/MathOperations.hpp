/*!
 * \file
 * \author David Saxon
 * \brief Generic math operations.
 *
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ARCANECORE_BASE_MATH_OPERATIONS_HPP_
#define ARCANECORE_BASE_MATH_OPERATIONS_HPP_

#include <cstdint>
#include <cstdlib>
#include <cmath>
#include <cstring>

#include "arcanecore/base/BaseAPI.hpp"
#include "arcanecore/base/math/MathConstants.hpp"


namespace arc
{
ARC_BASE_VERSION_NS_BEGIN
namespace math
{

/*!
 * \brief Clamps the given value so that it is greater than or equal to the
 *        threshold.
 */
template<typename T_data>
inline T_data clamp_above(T_data v, T_data threshold)
{
    if(v <= threshold)
    {
        return threshold;
    }
    return v;
}

/*!
 * \brief Clamps the given value so that it is less than or equal to the
 *        threshold.
 */
template<typename T_data>
inline T_data clamp_below(T_data v, T_data threshold)
{
    if(v >= threshold)
    {
        return threshold;
    }
    return v;
}

/*!
 * \brief Clamps the given value so that it is greater than or equal to the
 *        lower threshold and less than or equal to the upper threshold.
 *
 * \warning If upper_threshold is less than lower_threshold the result of this
 *          function is undefined.
 */
template<typename T_data>
inline T_data clamp(T_data v, T_data lower_threshold, T_data upper_threshold)
{
    if(v <= lower_threshold)
    {
        return lower_threshold;
    }
    if(v >= upper_threshold)
    {
        return upper_threshold;
    }

    return v;
}

/*!
 * \brief Calculates the exponent a raised to the power of b.
 *
 * \warning This is a performance orientated version of power that does not
 *          support negative exponents.
 */
template<typename T_data>
T_data pow_fast(T_data a, T_data b)
{
    return std::exp(b * std::log(a));
}

/*!
 * \brief Computes the reciprocal (or multiplicative inverse) of the square
 *        root for the given value.
 *
 * \note If T_data is float this will use the Fast Inverse Square Root to
 *       perform the calculation.
 */
template<typename T_data>
inline T_data rsqrt(T_data v)
{
    return static_cast<T_data>(1) / static_cast<T_data>(std::sqrt(v));
}

//---------------T E M P L A T E -- S P E C I A L I S A T I O N S---------------

template<>
inline float rsqrt(float v)
{
    // straight from Quake III Arena
    static const float threehalfs = 1.5F;

    float x2 = v * 0.5F;
    float y  = v;
    // evil floating point bit level hacking
    uint32_t i;
    std::memcpy(&i, &y, sizeof(float));
    // what the fuck?
    i  = 0x5f3759DF - (i >> 1);
    std::memcpy(&y, &i, sizeof(float));
    // y  = *((float*) &i);
    // 1st iteration
    y  = y * (threehalfs - (x2 * y * y));
    // 2nd iteration, this can be removed
    return y;
}

//------------------------------------------------------------------------------

/*!
 * \brief Converts the given value from degrees to radians.
 */
template<typename T_data>
inline T_data degrees_to_radians(T_data degrees)
{
    return static_cast<T_data>(DEGREES_TO_RADIANS) * degrees;
}

/*!
 * \brief Converts the given value from radians to degrees.
 */
template<typename T_data>
inline T_data radians_to_degrees(T_data radians)
{
    return static_cast<T_data>(RADIANS_TO_DEGREES) * radians;
}

} // namespace math
ARC_BASE_VERSION_NS_END
} // namespace arc

#endif
