/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ARCANECORE_BASE_REFLECT_REFLECTIONOPERATIONS_HPP_
#define ARCANECORE_BASE_REFLECT_REFLECTIONOPERATIONS_HPP_

#include <string>
#include <typeinfo>

#ifdef __GNUC__
    #include <cxxabi.h>
#endif

#include "arcanecore/base/BaseAPI.hpp"


namespace arc
{
ARC_BASE_VERSION_NS_BEGIN
namespace reflect
{

/*!
 * \brief Returns a string representing the given template type.
 *
 * For example:
 *
 * \code
 * arc::reflect::get_typename<int>();
 * \endcode
 *
 * Would return "int".
 *
 * \note The result of this function is not guaranteed to be consistent across
 *       different compilers.
 */
template<typename Type>
std::string get_typename()
{
#ifdef __GNUC__

    std::string ret("TYPE UNKNOWN");
    int status;
    char* demangled_name =
        abi::__cxa_demangle(typeid(Type).name(), NULL, NULL, &status);
    if(status == 0)
    {
        ret = demangled_name;
        std::free(demangled_name);
    }
    return ret;

#else

    return typeid(Type).name();

#endif
}

} // namespace reflect
ARC_BASE_VERSION_NS_END
} // namespace arc

#endif
