/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "arcanecore/base/dl/DLOperations.hpp"
#include "arcanecore/base/fsys/FileSystemOperations.hpp"


namespace arc
{
ARC_BASE_VERSION_NS_BEGIN
namespace dl
{

Handle open_library(arc::fsys::Path const& path)
{
    if(!arc::fsys::exists(path))
    {
        throw arc::ex::IOError(
            "Path to library does not exist: \"" + path.to_native() + "\""
        );
    }

    Handle handle = nullptr;

    #ifdef ARC_OS_WINDOWS

        // TODO: Unicode support: LoadLibraryW
        // get the handle
        handle = LoadLibrary(path.to_native().c_str());

    #elif defined(ARC_OS_UNIX)

        // TODO: support flags (i.e. RTLD_NOW)
        // get the handle
        handle = dlopen(path.to_native().c_str(), RTLD_LAZY);
        if(handle == nullptr)
        {
            throw arc::ex::DynamicLibraryError(dlerror());
        }

    #else

        throw arc::ex::NotImplementedError(
            "open_library has not been implemented for this platform."
        );

    #endif

    // check error
    if(handle == nullptr)
    {
        throw arc::ex::DynamicLibraryError(
            arc::os::get_last_system_error_message()
        );
    }

    return handle;
}

void close_library(Handle handle)
{
    bool error = false;

    #ifdef ARC_OS_WINDOWS

        error = !FreeLibrary((HMODULE) handle);

    #elif defined(ARC_OS_UNIX)

        if(dlclose(handle) != 0)
        {
            throw arc::ex::DynamicLibraryError(dlerror());
        }

    #else

        throw arc::ex::NotImplementedError(
            "close_library has not been implemented for this platform."
        );

    #endif

    // check error
    if(error)
    {
        throw arc::ex::DynamicLibraryError(
            arc::os::get_last_system_error_message()
        );
    }
}

} // namespace dl
ARC_BASE_VERSION_NS_END
} // namespace arc
