/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ARCANECORE_BASE_DL_DLOPERATIONS_HPP_
#define ARCANECORE_BASE_DL_DLOPERATIONS_HPP_

#include <string>

#include "arcanecore/base/BaseAPI.hpp"
#include "arcanecore/base/Exceptions.hpp"
#include "arcanecore/base/Preproc.hpp"
#include "arcanecore/base/fsys/Path.hpp"
#include "arcanecore/base/os/OSOperations.hpp"
#include "arcanecore/base/reflect/ReflectionOperations.hpp"


//------------------------------------------------------------------------------
//                                     MACROS
//------------------------------------------------------------------------------

/*!
 * \brief Directive used to export a function in a dynamic library.
 */
#define ARC_DL_EXPORT
#ifdef ARC_OS_WINDOWS
#   include <windows.h>
#   undef ARC_DL_EXPORT
#   define ARC_DL_EXPORT __declspec(dllexport)
#elif defined(ARC_OS_UNIX)
#   include <dlfcn.h>
#   undef ARC_DL_EXPORT
#   define ARC_DL_EXPORT __attribute__((visibility("default")))
#endif

namespace arc
{
ARC_BASE_VERSION_NS_BEGIN
namespace dl
{

//------------------------------------------------------------------------------
//                                TYPE DEFINITIONS
//------------------------------------------------------------------------------

/*!
 * \brief A handle to a loaded dynamic library.
 */
typedef void* Handle;

//------------------------------------------------------------------------------
//                                   FUNCTIONS
//------------------------------------------------------------------------------

/*!
 * \brief Opens the dynamic library from the given file path.
 *
 * \param path The path to the library.
 * \return The handle for the loaded library.
 *
 * \throws arc::ex::IOError If the given path does not exist.
 * \throws arc::ex::DynamicLibraryError If the file cannot be opened as a
 *                                      dynamic library.
 */
Handle open_library(arc::fsys::Path const& path);

/*!
 * \brief Closes the library pointed to by the given handle.
 *
 * \param handle The handle to the library to close.
 *
 * \throws arc::ex::DynamicLibraryError If the library failed to close.
 */
void close_library(Handle handle);

/*!
 * \brief Binds a symbol from the loaded dynamic library.
 *
 * \tparam T_Symbol The type of the symbol to bind from the library.
 *
 * \param handle The handle to the loaded dynamic library to bind the symbol
 *               from.
 * \param name The name of the symbol bind from the dynamic library.
 *
 * \throws arc::ex::DynamicLibraryError If the symbol could not be bound.
 */
template<typename T_Symbol>
T_Symbol* bind_symbol(Handle handle, std::string const& name)
{
    void* symbol_handle = nullptr;

    #ifdef ARC_OS_WINDOWS

        // TODO: support Unicode function names
        // get the handle
        symbol_handle = GetProcAddress((HMODULE) handle, name.c_str());

    #elif defined(ARC_OS_UNIX)

        // get the handle
        symbol_handle = dlsym(handle, name.c_str());
        if(symbol_handle == nullptr)
        {
            throw arc::ex::DynamicLibraryError(dlerror());
        }

    #else

        throw arc::ex::NotImplementedError(
            "bind_symbol has not been implemented for this platform."
        );

    #endif

    // check error
    if(symbol_handle == nullptr)
    {
        throw arc::ex::DynamicLibraryError(
            arc::os::get_last_system_error_message()
        );
    }

    // attempt to cast handle
    T_Symbol* symbol = reinterpret_cast<T_Symbol*>(
        reinterpret_cast<intptr_t>(symbol_handle)
    );
    if(symbol == nullptr)
    {
        throw arc::ex::DynamicLibraryError(
            "Cannot bind symbol: \"" + name + "\" as the requested type: <" +
            arc::reflect::get_typename<T_Symbol>() + ">"
        );
    }

    // done
    return symbol;
}

} // namespace dl
ARC_BASE_VERSION_NS_END
} // namespace arc

#endif
