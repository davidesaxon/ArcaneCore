/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "arcanecore/base/Exceptions.hpp"
#include "arcanecore/base/Preproc.hpp"
#include "arcanecore/base/fsys/Path.hpp"
#include "arcanecore/base/str/StringOperations.hpp"

#ifdef ARC_OS_UNIX

    #include <limits.h>
    #include <unistd.h>

#elif defined(ARC_OS_WINDOWS)

    #include <windows.h>

#endif


namespace arc
{
ARC_BASE_VERSION_NS_BEGIN
namespace fsys
{

//------------------------------------------------------------------------------
//                                  CONSTRUCTORS
//------------------------------------------------------------------------------

Path::Path()
{
}

Path::Path(std::vector<std::string> const& components)
    : m_components(components)
{
}

Path::Path(std::string_view const& string_path)
{
    #ifdef ARC_OS_WINDOWS

        Path temp = from_posix_string(string_path);

    #else

        Path temp = from_windows_string(string_path);

    #endif

    m_components = std::move(temp.m_components);
}

Path::Path(Path const& other)
    : m_components(other.m_components)
{
}

Path::Path(Path&& other)
    : m_components(std::move(other.m_components))
{
}

//------------------------------------------------------------------------------
//                                   DESTRUCTOR
//------------------------------------------------------------------------------

Path::~Path()
{
}

//------------------------------------------------------------------------------
//                            PUBLIC STATIC FUNCTIONS
//------------------------------------------------------------------------------

Path Path::from_posix_string(std::string_view const& string_path)
{
    std::vector<std::string> const components =
        arc::str::split(string_path, "/");

    std::vector<std::string> path_components;
    path_components.reserve(components.size());
    for(std::string const& c : components)
    {
        if(c.empty())
        {
            continue;
        }
        path_components.push_back(c);
    }

    return Path(path_components);
}

Path Path::from_windows_string(std::string_view const& string_path)
{
    std::vector<std::string> const components =
        arc::str::split(string_path, "\\");

    std::vector<std::string> path_components;
    path_components.reserve(components.size());
    for(std::string const& c : components)
    {
        if(c.empty())
        {
            continue;
        }
        path_components.push_back(c);
    }

    return Path(path_components);
}

//------------------------------------------------------------------------------
//                                   OPERATORS
//------------------------------------------------------------------------------

Path& Path::operator=(Path const& other)
{
    m_components = other.m_components;

    return *this;
}

Path Path::operator=(Path&& other)
{
    m_components = std::move(other.m_components);

    return *this;
}

bool Path::operator==(Path const& other) const
{
    if(m_components.size() != other.m_components.size())
    {
        return false;
    }
    for(std::size_t i = 0; i < m_components.size(); ++i)
    {
        if(m_components[i] != other.m_components[i])
        {
            return false;
        }
    }
    return true;
}

bool Path::operator!=(Path const& other) const
{
    return !((*this) == other);
}

bool Path::operator<(Path const& other) const
{
    // check size first
    if(m_components.size() != other.m_components.size())
    {
        return m_components.size() < other.m_components.size();
    }
    // evaluate on each components
    for(std::size_t i = 0; i < m_components.size(); ++i)
    {
        if(m_components[i] != other.m_components[i])
        {
            return m_components[i] < other.m_components[i];
        }
    }
    return false;
}

std::string const& Path::operator[](std::size_t index)
{
    return m_components[index];
}

const std::string& Path::operator[](std::size_t index) const
{
    return m_components[index];
}

Path Path::operator+(Path const& other) const
{
    // create a new path which is a copy of this path
    Path copy(*this);
    // now use compound operator
    return copy += other;
}

Path& Path::operator+=(Path const& other)
{
    m_components.reserve(m_components.size() + other.m_components.size());
    for(std::string const& c : other.m_components)
    {
        m_components.push_back(c);
    }

    return *this;
}

Path& Path::operator<<(std::string_view const& component)
{
    return join(component);
}

//------------------------------------------------------------------------------
//                            PUBLIC MEMBER FUNCTIONS
//------------------------------------------------------------------------------

Path& Path::join(std::string_view const& component)
{
    m_components.emplace_back(component);
    return *this;
}

void Path::insert(std::size_t index, std::string_view const& component)
{
    // check index
    if(index > m_components.size())
    {
        throw arc::ex::IndexError(
            "Attempted to insert component \"" + std::string(component) +
            "\" at index " + std::to_string(index) + " which is greater than "
            "the number of components currently in the path."
        );
    }

    // create a new vector
    std::vector<std::string> new_components;
    new_components.reserve(m_components.size() + 1);
    // copy over
    for(std::size_t i = 0; i < m_components.size() + 1; ++i)
    {
        if(i < index)
        {
            new_components.emplace_back(std::move(m_components[i]));
        }
        else if(i > index)
        {
            new_components.emplace_back(std::move(m_components[i - 1]));
        }
        else
        {
            new_components.emplace_back(component);
        }
    }
    // replace
    m_components = new_components;
}

void Path::remove(std::size_t index)
{
    // check index
    if(index >= m_components.size())
    {
        throw arc::ex::IndexError(
            "Attempted to remove component from index " +
            std::to_string(index) + " which is greater or equal to the number "
            "of components currently in the path."
        );
    }

    // create a new vector
    std::vector<std::string> new_components;
    new_components.reserve(m_components.size() - 1);
    // copy over
    for(std::size_t i = 0; i < m_components.size(); ++i)
    {
        if(i != index)
        {
            new_components.emplace_back(std::move(m_components[i]));
        }
    }
    // replace
    m_components = new_components;
}

void Path::clear()
{
    m_components.clear();
}

std::string Path::to_native() const
{
    #ifdef ARC_OS_WINDOWS

        return to_windows();

    #else

        return to_posix();

    #endif
}

std::string Path::to_posix() const
{
    if(m_components.empty())
    {
        return "";
    }

    std::vector<std::string> components;
    // special case for root (/)
    bool is_root = false;
    if(m_components.front() == "/")
    {
        is_root = true;
        components = std::vector<std::string>(
            m_components.begin() + 1,
            m_components.end()
        );
    }
    else
    {
        components = m_components;
    }

    std::string ret = arc::str::join(
        components.begin(),
        components.end(),
        "/"
    );
    if(is_root)
    {
        ret = "/" + ret;
    }

    return ret;
}

std::string Path::to_windows() const
{
    return arc::str::join(
        m_components.begin(),
        m_components.end(),
        "\\"
    );
}

Path Path::to_absolute() const
{
    // TODO: resolve . and ..

    #ifdef ARC_OS_UNIX

        // if the path is already from root just return a copy of it
        if(!empty() && front() == "/")
        {
            return Path(*this);
        }

        // otherwise get the current working directory and prepend it to this
        // path
        char cwd[PATH_MAX];
        getcwd(cwd, PATH_MAX);
        // create a new path and append
        Path abs_path(cwd);
        abs_path += *this;
        return abs_path;

    #elif defined(ARC_OS_WINDOWS)

        std::string const native = to_windows();
        char full_path[MAX_PATH];
        GetFullPathNameA(
            (LPCSTR) native.c_str(),
            MAX_PATH,
            full_path,
            nullptr
        );
        return Path(full_path);

    #else

        throw arc::test::ex::NotImplementedError(
            "Path::to_absolute() is not yet supported for the current platform"
        );

    #endif
}

std::size_t Path::size() const
{
    return m_components.size();
}

bool Path::empty() const
{
    return m_components.empty();
}

std::string const& Path::front() const
{
    return m_components.front();
}

std::string const& Path::back() const
{
    return m_components.back();
}

std::vector<std::string> const& Path::get_components() const
{
    return m_components;
}

std::string Path::get_extension() const
{
    // is there a final component?
    if(!m_components.empty())
    {
        // does the final component contain a period?
        std::size_t loc = m_components.back().rfind(".");
        if(loc != std::string::npos)
        {
            // return the extension substring
            return m_components.back().substr(
                    loc + 1,
                    m_components.back().length()
            );
        }
    }
    // there is no extension, return an empty string
    return "";
}

//------------------------------------------------------------------------------
//                               EXTERNAL OPERATORS
//------------------------------------------------------------------------------

std::ostream& operator<<(std::ostream& stream, const Path& p)
{
    stream << p.to_native();
    return stream;
}

} // namespace fsus
ARC_BASE_VERSION_NS_END
} // namespace arc
