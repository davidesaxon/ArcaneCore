/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ARCANECORE_BASE_FSYS_FILESYSTEMOPERATIONS_HPP_
#define ARCANECORE_BASE_FSYS_FILESYSTEMOPERATIONS_HPP_

#include <vector>

#include "arcanecore/base/BaseAPI.hpp"
#include "arcanecore/base/fsys/Path.hpp"


namespace arc
{
ARC_BASE_VERSION_NS_BEGIN
namespace fsys
{

/*!
 * \brief Checks whether the given path exists on the file system.
 *
 * This operation does not concern with the type of the path, this will return
 * true if the path is a directory, file, symbolic link, or hard link.
 *
 * \param path The path to check for existence.
 * \param resolve_links Controls whether the operation will follow symbolic
 *                      links and treat them as the path it points to or if the
 *                      path to a symbolic link should be treated as the
 *                      absolute path to the link itself (default behavior).
 *                      This argument has no effect on Windows platforms since
 *                      symbolic linking is not supported by Windows.
 * \return Returns True if the path exists, false otherwise.
 */
bool exists(arc::fsys::Path const& path, bool resolve_links = false);

/*!
 * \brief Returns whether the given path is a regular file.
 *
 * \note If the path does not exist this operation will return false.
 *
 * \param resolve_links Controls whether the operation will follow symbolic
 *                      links and treat them as the path it points to or if the
 *                      path to a symbolic link should be treated as the
 *                      absolute path to the link itself (default behavior).
 *                      This argument has no effect on Windows platforms since
 *                      symbolic linking is not supported by Windows.
 * \return Returns True if the given path is a file, false otherwise.
 */
bool is_file(arc::fsys::Path const& path, bool resolve_links = false);

/*!
 * \brief Returns whether the given path is a directory.
 *
 * \note If the path does not exist this operation will return false.
 *
 * \param resolve_links Controls whether the operation will follow symbolic
 *                      links and treat them as the path it points to (default
 *                      behavior) or if the path to a symbolic link should be
 *                      treated as the absolute path to the link itself. This
 *                      argument has no effect on Windows platforms since
 *                      symbolic linking is not supported by Windows.
 * \return Returns True if the given path is a directory, false otherwise.
 */
bool is_directory(arc::fsys::Path const& path, bool resolve_links = false);

/*!
 * \brief Returns whether the given path is a symbolic link.
 *
 * \note If the path does not exist this operation will return false.
 */
bool is_symbolic_link(arc::fsys::Path const& path);

/*!
 * \brief Lists the file system paths located under the given path.
 *
 * This operation performs a similar function to the `ls` command on Linux, or
 * the `dir` command on Windows. An empty vector will be returned if the given
 * path does not exist or is not a directory. This function will not resolve
 * a symbolic link if one is provided as the input path.
 *
 * \param path The file path to list sub-paths for.
 * \param include_special Whether the special symbols "." and ".." while be
 *                        included with the returned paths.
 */
std::vector<arc::fsys::Path> list(
        arc::fsys::Path const& path,
        bool include_special = false);

/*!
 * \brief Lists all descendant file system paths located under the given path.
 *
 * This operation returns a vector similar to that of list(), except any sub
 * paths that are also directories are traversed and so on, so that this
 * function returns all paths that are a descendant of the given path. This
 * function will not resolve symbolics in order to avoid infinite recursion.
 */
std::vector<arc::fsys::Path> list_rec(
        arc::fsys::Path const& path,
        bool include_special = false);

/*!
 * \brief Attempts to create the directory at the given path.
 *
 * \throws arc::ex::IOError If directory creation was attempted but failed.
 *
 * \return True is a new directory was created, false if the directory already
 *         exists.
 */
bool create_directory(arc::fsys::Path const& path);

/*!
 * \brief Deletes the given Path on the file system if it exists.
 *
 * This operation will fail if the given path is a non-empty directory, see
 * arc::fsys::delete_path_rec() to delete non-empty directories.
 *
 * \note This operation will delete symbolic link objects but will not follow
 *       them to delete the path they point to.
 *
 * \throws arc::ex::IOError If the path cannot be accessed and/or modified to be
 *                          deleted.
 */
void delete_path(arc::fsys::Path const& path);

/*!
 * \brief Deletes the given path on the file system and all subsequent paths
 *        under the given path.
 *
 * This effectively performs the same task as arc::fsys::delete_path() except
 * that non-empty directories and their contents will also be deleted by this
 * operation.
 *
 * \note This operation will delete symbolic link objects but will not follow
 *       them to delete the path they point to.
 *
 * \throws arc::ex::IOError If a path in the directory hierarchy cannot be
 *                          accessed and/or modified to be deleted.
 */
void delete_path_rec(arc::fsys::Path const& path);

/*!
 * \brief Attempts to ensure all directories up to the provided path exist.
 *
 * If any directories up to the final component of the provided path do not
 * exist this operation will attempt to create them. If the given path cannot be
 * validated this will fail with an exception.
 *
 * This operation is useful to ensure a path is available for writing.
 *
 * Example usage:
 *
 * \code
 * arc::fsys::Path p({"example", "path", "to", "filename.txt"});
 * arc::fsys::validate(p);
 * // this will attempt to ensure example/path/to/ exists, if this operations
 * // failed an exception would have been thrown.
 * // We can now begin writing to filename.txt knowing that it's containing
 * // directory exists.
 * std::ofstream open_file(p.to_native().c_str());
 *
 * \endcode
 *
 * \throws arc::ex::IOError If one of the directories in the path was attempted
 *                          to be created but failed.
 */
 void validate(arc::fsys::Path const& path);

} // namespace fsys
ARC_BASE_VERSION_NS_END
} // namespace arc

#endif
