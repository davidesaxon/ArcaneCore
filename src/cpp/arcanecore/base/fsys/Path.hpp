/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ARCANECORE_BASE_FSYS_PATH_HPP_
#define ARCANECORE_BASE_FSYS_PATH_HPP_

#include <string>
#include <vector>

#include <arcanecore/cxx17/string_view.hpp>

#include "arcanecore/base/BaseAPI.hpp"


namespace arc
{
ARC_BASE_VERSION_NS_BEGIN
namespace fsys
{

/*!
 * \brief Represents a platform independent file system path.
 */
class Path
{
public:

    //--------------------------------------------------------------------------
    //                                CONSTRUCTORS
    //--------------------------------------------------------------------------

    /*!
     * \brief Construct a new empty file system path.
     */
    Path();

    /*!
     * \brief Constructs a new file system path compromised of the given
     *        components.
     *
     * For example the native Linux path:
     *
     * \code
     * path/to/file.txt
     * \endcode
     *
     * Would be passed to this constructor like so:
     *
     * \code
     * arc::io::file::Path p({"path", "to", "file.txt"});
     * \endcode
     *
     * While a path from root such as:
     *
     * \code
     * /path/to/file.txt
     * \endcode
     *
     * Would be passed to this constructor like so:
     *
     * \code
     * arc::io::file::Path p({"/", "path", "to", "file.txt"});
     * \endcode
     */
    Path(std::vector<std::string> const& components);

    /*!
     * \brief Constructs a new file system path compromised of the components
     *        yielded by the given iterators.
     */
    template<typename T_InputIterator>
    Path(T_InputIterator const& first, T_InputIterator const& last)
        : m_components(first, last)
    {
    }

    /*!
     * \brief Attempts to create a Path object from the given single string
     *        using the current operating system's path rules.
     *
     * \note This constructor method should be avoided where possible.
     *
     * On Posix systems this will split the given string into components using
     * the "/" symbol. Likewise on Windows systems the "\" symbol will be used.
     */
    Path(std::string_view const& string_path);

    /*!
     * \brief Copy constructor.
     */
    Path(Path const& other);

    /*!
     * \brief Move constructor.
     */
    Path(Path&& other);

    //--------------------------------------------------------------------------
    //                                 DESTRUCTOR
    //--------------------------------------------------------------------------

    ~Path();

    //--------------------------------------------------------------------------
    //                          PUBLIC STATIC FUNCTIONS
    //--------------------------------------------------------------------------

    /*!
     * \brief Creates a Path object by splitting a Posix style string on the /
     *        character.
     */
    static Path from_posix_string(std::string_view const& string_path);

    /*!
     * \brief Creates a Path object by splitting a Windows style string on the \
     *        character.
     */
    static Path from_windows_string(std::string_view const& string_path);

    //--------------------------------------------------------------------------
    //                                 OPERATORS
    //--------------------------------------------------------------------------

    /*!
     * \brief Copy assignment operator.
     */
    Path& operator=(Path const& other);

    /*!
     * \brief Move assignment operator.
     */
    Path operator=(Path&& other);

    /*!
     * \brief Equality operator.
     */
    bool operator==(Path const& other) const;

    /*!
     * \brief Inequality operator.
     */
    bool operator!=(Path const& other) const;

    /*!
     * \brief Less than operator.
     *
     * Compares whether this Path is considered less than the other given Path.
     *
     * \note This operator is primarily supplied so Path objects can be stored
     *       in a std::map.
     *
     * Less than is defined by the number of components in a Path, and if two
     * paths have the same number of components then this resorts to a
     * std::string less than comparison on each component until a mismatch is
     * found.
     */
    bool operator<(Path const& other) const;

    /*!
     * \brief Returns a reference to the component of this Path at the given
     *        index.
     *
     * \throws arc::ex::IndexError If the provided index is out of range of the
     *                             number of components in this Path.
     */
    std::string const& operator[](std::size_t index);

    /*!
     * \brief Returns a const reference to the component of this Path at the
     *        given index.
     *
     * \throws arc::ex::IndexError If the provided index is out of range of the
     *                             number of components in this Path.
     */
    const std::string& operator[](std::size_t index) const;

    /*!
     * \brief Addition operator.
     *
     * Creates a new path by extending the components of this path with the
     * components of the other given path.
     *
     * Example usage:
     *
     * \code
     * arc::fsys::Path p1({"two", "part"});
     *
     * arc::fsys::Path p2({"path", "to", "file.txt"});
     *
     * arc::fsys::Path p3 = p1 + p2;
     * // p3 contains ["two", "part", "path", "to", "file.txt"]
     * \endcode
     */
    Path operator+(Path const& other) const;


    /*!
     * \brief Compound addition operator.
     *
     * Extends the components of this path with the components of the other
     * given path.
     *
     * Example usage:
     *
     * \code
     * arc::fsys::Path p1({"two", "part"});
     *
     * arc::fsys::Path p2({"path", "to", "file.txt"});
     *
     * p1 += p2;
     * // p1 contains ["two", "part", "path", "to", "file.txt"]
     * \endcode
     */
    Path& operator+=(Path const& other);

    /*!
     * \brief Join operator.
     *
     * Appends a new component to the end of this path - the same behavior as
     * the join function.
     *
     * Example usage:
     *
     * \code
     * arc::fsys::Path p;
     * p << "path" << "to" << "file.txt";
     * // p now contains {"path", "to", "file.txt"}
     * \endcode
     */
    Path& operator<<(std::string_view const& component);

    //--------------------------------------------------------------------------
    //                          PUBLIC MEMBER FUNCTIONS
    //--------------------------------------------------------------------------

    /*!
     * \brief Appends a new component to the end of this path.
     * This has the same behavior as the << operator.
     *
     * Example usage:
     *
     * \code
     * arc::fsys::Path p;
     * p.join("path").join("to").join("file.txt");
     * // p now contains {"path", "to", "file.txt"}
     * \endcode
     */
    Path& join(std::string_view const& component);

    /*!
     * \brief Inserts the component at the given index in this path.
     *
     * \throws arc::ex::IndexError If the provided index is greater than the
     *                             number of components currently in this Path.
     */
    void insert(std::size_t index, std::string_view const& component);

    /*!
     * \brief Removes the component at the given index from this path.
     *
     * \throws arc::ex::IndexError If the provided index is out of range of the
     *                             number of components in this Path.
     */
    void remove(std::size_t index);

    /*!
     * \brief Removes all components from this path.
     */
    void clear();

    /*!
     * \brief Returns a single string representation of this path for the
     *        current operating system.
     *
     * Example usage:
     *
     * \code
     * arc::fsys::Path p({"path", "to", "file.txt"});
     * std::string const string_path = p.to_native();
     * \endcode
     *
     * On Posix systems string_path will be "path/to/file.txt" while on Windows
     * systems it will be "path\to\file.txt".
     */
    std::string to_native() const;

    /*!
     * \brief Returns a single string representation of this path for Posix
     *        systems.
     *
     * Example usage:
     *
     * \code
     * arc::fsys::Path p({"path", "to", "file.txt"});
     * std::string const string_path = p.to_posix();
     * \endcode
     *
     * string_path will be "path/to/file.txt".
     */
    std::string to_posix() const;

    /*!
     * \brief Returns a single string representation of this path for Windows
     *        systems.
     *
     * Example usage:
     *
     * \code
     * arc::fsys::Path p({"path", "to", "file.txt"});
     * std::string const string_path = p.to_posix();
     * \endcode
     *
     * string_path will be "path\to\file.txt".
     */
    std::string to_windows() const;

    /*!
     * \brief Returns a new Path object the represents the absolute version of
     *        this path.
     */
    Path to_absolute() const;

    /*!
     * \brief Returns the number of components in this path.
     */
    std::size_t size() const;

    /*!
     * \brief Returns whether this path has no components or not.
     */
    bool empty() const;

    /*!
     * \brief Returns the first component of this Path.
     *
     * \throws arc::ex::IndexError If this Path is empty.
     */
    std::string const& front() const;

    /*!
     * \brief Returns the last component of this Path.
     *
     * \throws arc::ex::IndexError If this Path is empty.
     */
    std::string const& back() const;

    /*!
     * \brief Returns the component of this Path as a std::vector.
     */
    std::vector<std::string> const& get_components() const;

    /*!
     * \brief Returns the file extension of the last component of this path.
     *
     * Returns "" if the Path is empty or the extension cannot be determined.
     */
    std::string get_extension() const;

private:

    //--------------------------------------------------------------------------
    //                             PRIVATE ATTRIBUTES
    //--------------------------------------------------------------------------

    // the components of this Path
    std::vector<std::string> m_components;
};

//------------------------------------------------------------------------------
//                               EXTERNAL OPERATORS
//------------------------------------------------------------------------------

std::ostream& operator<<(std::ostream& stream, const Path& p);

} // namespace fsys
ARC_BASE_VERSION_NS_END
} // namespace arc

//------------------------------------------------------------------------------
//                                      HASH
//------------------------------------------------------------------------------

namespace std
{

template<>
struct hash<arc::fsys::Path> :
    public unary_function<arc::fsys::Path, size_t>
{
    std::size_t operator()(const arc::fsys::Path& value) const
    {
        // TODO: could use custom faster hash here
        std::hash<std::string> hasher;
        return hasher(value.to_posix());
    }
};

} // namespace std

#endif
