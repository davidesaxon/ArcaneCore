/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "arcanecore/base/Preproc.hpp"

#include <algorithm>
#include <sys/stat.h>
#include <sys/types.h>

#ifdef ARC_OS_UNIX

 #include <dirent.h>
 #include <unistd.h>

#elif defined(ARC_OS_WINDOWS)

 #include <windows.h>

#endif

#include "arcanecore/base/Exceptions.hpp"
#include "arcanecore/base/fsys/FileSystemOperations.hpp"
#include "arcanecore/base/os/OSOperations.hpp"


namespace arc
{
ARC_BASE_VERSION_NS_BEGIN
namespace fsys
{

bool exists(arc::fsys::Path const& path, bool resolve_links)
{
    #ifdef ARC_OS_UNIX

        // stat or lstat the file depending on whether we want to resolve links or
        // not
        struct stat s;
        int stat_ret = false;
        if(resolve_links)
        {
            stat_ret = stat(path.to_posix().c_str(), &s);
        }
        else
        {
            stat_ret = lstat(path.to_posix().c_str(), &s);
        }

        // return based on the stat return code
        return stat_ret == 0;

    #elif defined(ARC_OS_WINDOWS)

        // TODO: add support for Unicode file paths
        // struct _stat64i32 s;
        // int result =  _wstat64i32((const wchar_t*) p, &s);

        struct _stat s;
        int result =  _stat(path.to_windows().c_str(), &s);

        if(result == 0)
        {
            return true;
        }
        return false;

    #else

        throw arc::ex::NotImplementedError(
            "arc::fsys::exists has not yet been implemented for this platform"
        );

    #endif
}

bool is_file(arc::fsys::Path const& path, bool resolve_links)
{
    #ifdef ARC_OS_UNIX

        // stat or lstat the file depending on whether we want to resolve links or
        // not
        struct stat s;
        int stat_ret = false;
        if(resolve_links)
        {
            stat_ret = stat(path.to_posix().c_str(), &s);
        }
        else
        {
            stat_ret = lstat(path.to_posix().c_str(), &s);
        }

        if(stat_ret == 0)
        {
            if(S_ISREG(s.st_mode))
            {
                return true;
            }
        }
        return false;

    #elif defined(ARC_OS_WINDOWS)

        // TODO: add support for Unicode file paths
        // struct _stat64i32 s;
        // int result =  _wstat64i32((const wchar_t*) p, &s);

        struct _stat s;
        int result =  _stat(path.to_windows().c_str(), &s);

        if(result == 0)
        {
            if(s.st_mode & S_IFREG)
            {
                return true;
            }
        }
        return false;

    #else

        throw arc::ex::NotImplementedError(
            "arc::fsys::is_file has not yet been implemented for this platform"
        );

    #endif
}

bool is_directory(arc::fsys::Path const& path, bool resolve_links)
{
    #ifdef ARC_OS_UNIX

        // stat or lstat the file depending on whether we want to resolve links or
        // not
        struct stat s;
        int stat_ret = false;
        if(resolve_links)
        {
            stat_ret = stat(path.to_posix().c_str(), &s);
        }
        else
        {
            stat_ret = lstat(path.to_posix().c_str(), &s);
        }

        if(stat_ret == 0)
        {
            if(S_ISDIR(s.st_mode))
            {
                return true;
            }
        }
        return false;

    #elif defined(ARC_OS_WINDOWS)

        // TODO: add support for Unicode file paths
        // struct _stat64i32 s;
        // int result =  _wstat64i32((const wchar_t*) p, &s);

        struct _stat s;
        int result =  _stat(path.to_windows().c_str(), &s);

        if(result == 0)
        {
            if(s.st_mode & S_IFDIR)
            {
                return true;
            }
        }
        return false;

    #else

        throw arc::ex::NotImplementedError(
            "arc::fsys::is_directory has not yet been implemented for this "
            "platform"
        );

    #endif
}

bool is_symbolic_link(arc::fsys::Path const& path)
{
    #ifdef ARC_OS_UNIX

        struct stat s;
        if(lstat(path.to_posix().c_str(), &s) == 0)
        {
            if(S_ISLNK(s.st_mode))
            {
                return true;
            }
        }
        return false;

    #elif defined(ARC_OS_WINDOWS)

        // no symbolic links on Windows
        return false;

    #else

        throw arc::ex::NotImplementedError(
            "arc::fsys::is_symbolic_link has not yet been implemented for this "
            "platform"
        );

    #endif
}

std::vector<arc::fsys::Path> list(
        arc::fsys::Path const& path,
        bool include_special)
{
    // vector to be returned
    std::vector<arc::fsys::Path> ret;

    // is the given path a directory?
    if(!is_directory(path, false))
    {
        // not a directory so there are no sub-children
        return ret;
    }

    #ifdef ARC_OS_UNIX

        // open the directory
        DIR* dir;
        if((dir = opendir(path.to_posix().c_str())) == NULL)
        {
            // failed to open the directory
            throw arc::ex::IOError(
                "Failed to open directory to list contents: \"" +
                path.to_native() + "\""
            );
        }

        struct dirent* dir_entry;
        while((dir_entry = readdir(dir)) != NULL)
        {
            arc::fsys::Path p(path);
            p << dir_entry->d_name;

            // skip over . and ..?
            if(!include_special && (p.back() == "." || p.back() == ".."))
            {
                continue;
            }

            ret.push_back(p);
        }
        closedir(dir);

    #elif defined(ARC_OS_WINDOWS)

        // TODO: add support for Unicode file paths
        // WIN32_FIND_DATAW find_data;
        // HANDLE find_handle = FindFirstFileW((const wchar_t*) p, &find_data);

        std::string built_path = path.to_windows() + "\\*";

        WIN32_FIND_DATAA find_data;
        HANDLE find_handle = FindFirstFileA(
            // path.to_windows().c_str(),
            built_path.c_str(),
            &find_data
        );

        if(find_handle == INVALID_HANDLE_VALUE)
        {
            // failed to open the directory
            throw arc::ex::IOError(
                "Failed to open directory to list contents: \"" +
                path.to_native() + "\""
            );
        }

        // iterate over sub paths
        do
        {
            arc::fsys::Path sub_path(path);
            sub_path << find_data.cFileName;

            // skip over . and ..?
            if(!include_special &&
               (sub_path.back() == "." || sub_path.back() == ".."))
            {
                continue;
            }

            ret.push_back(sub_path);
        }
        while (FindNextFileA(find_handle, &find_data) != 0);
        FindClose(find_handle);

    #else

        throw arc::ex::NotImplementedError(
            "arc::fsys::list has not yet been implemented for this platform"
        );

    #endif

    return ret;
}

std::vector<arc::fsys::Path> list_rec(
        arc::fsys::Path const& path,
        bool include_special)
{
    std::vector<arc::fsys::Path> ret;

    for(arc::fsys::Path const& sub_path : list(path, include_special))
    {
        ret.push_back(sub_path);
        // recursively call and extend the return list
        if(sub_path.back() != "." && sub_path.back() != "..")
        {
            for(arc::fsys::Path const& sub_sub_path :
                list_rec(sub_path, include_special))
            {
                ret.push_back(sub_sub_path);
            }
        }
    }

    return ret;
}

bool create_directory(arc::fsys::Path const& path)
{
    // does the path already exist?
    if(exists(path))
    {
        // is this an ambiguous path?
        if(!is_directory(path))
        {
            throw arc::ex::IOError(
                "Directory creation failed as the path already exists but is "
                "not a directory \"" + path.to_native() + "\""
            );
        }
        // no actions performed
        return false;
    }

    #ifdef ARC_OS_UNIX

        if(mkdir(path.to_posix().c_str(), 0777) != 0)
        {
            throw arc::ex::IOError(
                "Directory creation failed with OS error: " +
                arc::os::get_last_system_error_message()
            );
        }

    #elif defined(ARC_OS_WINDOWS)

        // TODO: add support for Unicode file paths
        // BOOL result = CreateDirectoryW((const wchar_t*) p, NULL);

        BOOL result = CreateDirectoryA(path.to_windows().c_str(), NULL);

        if(!result)
        {
            throw arc::ex::IOError(
                "Directory creation failed with OS error: " +
                arc::os::get_last_system_error_message()
            );
        }

    #else

        throw arc::ex::NotImplementedError(
            "arc::fsys::create_directory has not yet been implemented for this "
            "platform"
        );

    #endif

    return true;
}

void delete_path(arc::fsys::Path const& path)
{
    // does the file exist?
    if(!exists(path))
    {
        throw arc::ex::IOError(
            "Cannot delete path because it does not exist: \"" +
            path.to_native() + "\""
        );
    }

    #ifdef ARC_OS_UNIX

        if(remove(path.to_posix().c_str()) != 0)
        {
            throw arc::ex::IOError(
                "Failed to delete path \"" + path.to_native() +
                "\". OS error: " + arc::os::get_last_system_error_message()
            );
        }

    #elif defined(ARC_OS_WINDOWS)

        // TODO: add support for Unicode file paths
        // if(is_file(path))
        // {
        //     result = DeleteFileW((const wchar_t*) p);
        // }
        // else if(is_directory(path))
        // {
        //     result = RemoveDirectoryW((const wchar_t*) p);
        // }

        BOOL result = 0;
        if(is_file(path))
        {
            result = DeleteFileA(path.to_windows().c_str());
        }
        else if(is_directory(path))
        {
            result = RemoveDirectoryA(path.to_windows().c_str());
        }

        if(!result)
        {
            throw arc::ex::IOError(
                "Failed to delete path \"" + path.to_native() +
                "\". OS error: " + arc::os::get_last_system_error_message()
            );
        }

    #else

        throw arc::ex::NotImplementedError(
            "arc::fsys::delete_path has not yet been implemented for this "
            "platform"
        );

    #endif
}

void delete_path_rec(arc::fsys::Path const& path)
{
    // does the file exist?
    if(!exists(path))
    {
        throw arc::ex::IOError(
            "Cannot delete path because it does not exist: \"" +
            path.to_native() + "\""
        );
    }

    // is this a directory? do we need to traverse it?
    if(is_directory(path))
    {
        for(arc::fsys::Path const& sub_path : list(path, false))
        {
            // skip . and ..
            if(sub_path.empty()       ||
               sub_path.back() == "." ||
               sub_path.back() == ".."  )
            {
                continue;
            }

            // follow paths
            delete_path_rec(sub_path);
        }
    }

    // delete the starting path
    delete_path(path);
}

void validate(arc::fsys::Path const& path)
{
    // do nothing if the path is too short
    if(path.size() < 2)
    {
        return;
    }

    // iterate over the path ensuring each path exists
    for(std::size_t i = 1; i < path.size(); ++i)
    {
        std::vector<std::string> c = path.get_components();
        create_directory(arc::fsys::Path(c.begin(), c.begin() + i));
    }
}

} // namespace fsys
ARC_BASE_VERSION_NS_END
} // namespace arc
