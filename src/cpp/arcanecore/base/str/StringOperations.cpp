/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <cstring>
#include <cctype>

#include "arcanecore/base/str/StringOperations.hpp"


namespace arc
{
ARC_BASE_VERSION_NS_BEGIN
namespace str
{

bool is_uint(std::string_view const& s)
{
    // an empty string is not a valid number
    if (s.empty())
    {
        return false;
    }

    // TODO: benchmark this and use proper alignment and word size
    static uint32_t const chunk_size = 4;
    // iterate over groups of bytes based on the chunk size
    uint32_t const chunks = static_cast<uint32_t>(s.length() / chunk_size);
    for(std::size_t i = 0; i < chunks; ++i )
    {
        uint32_t const x = *(uint32_t const*)(&s.data()[i * chunk_size]);
        if((x & 0xF0F0F0F0) != 0x30303030)
        {
            return false;
        }
        uint32_t const y = (x & 0x08080808);
        if (y && x & (y >> 1 | y >> 2) & 0x06060606)
        {
            return false;
        }
    }
    // iterate over what's left of the chunks
    for(std::size_t i = (chunks * chunk_size); i < s.length(); ++i)
    {
        if (!std::isdigit(s[i]))
        {
            return false;
        }
    }

    // we found nothing wrong with it
    return true;
}

bool is_int(std::string_view const& s)
{
    // an empty string is not a valid number
   if(s.empty())
   {
       return false;
   }

   // is the first character an inverse?
   if(s[0] == '-')
   {
       // hand off to the optimised unsigned check
       return is_uint(s.data() + 1);
   }
   // hand off to the optimised unsigned check
   return is_uint(s);
}

bool is_float(std::string_view const& s)
{
    // an empty string is not a valid number
    if(s.empty())
    {
        return false;
    }

    std::size_t i = 0;
    // is the first character an inverse?
    if(s[0] == '-')
    {
        i = 1;
        // there should be more data..
        if(s.length() == 1)
        {
            return false;
        }
    }

    // TODO: this can probably be optimized

    // check each character
    bool found_period = false;
    bool found_digits = false;
    for(; i < s.length(); ++i)
    {
        if(s[i] == '.' && !found_period)
        {
            // there needs to be digits on at least one side of the period
            if(!found_digits && i >= s.length() - 1)
            {
                return false;
            }

            found_period = true;
            continue;
        }
        // scientific notation support
        if(s[i] == 'e' && found_digits)
        {
            // there needs to be at least 3 characters (e-<digits>)
            if(i >= s.length() - 2)
            {
                return false;
            }
            // check for the sign character
            if(s[i + 1] != '+' && s[i + 1] != '-')
            {
                return false;
            }
            // check that the rest of the string is a unsigned int
            return is_uint(s.data() + i + 2);
        }
        // check digits
        if(!std::isdigit(s[i]))
        {
            return false;
        }
        found_digits = true;
    }

    // we found no errors
    return true;
}

bool is_whitespace(std::string_view const& s)
{
    // iterate over each character and check if it's whitespace
    for(std::size_t i = 0; i < s.length(); ++i)
    {
        if(!isspace(s[i]))
        {
            return false;
        }
    }
    return true;
}

bool starts_with(std::string_view const& a, std::string_view const& b)
{
    if(b.length() > a.length())
    {
        return false;
    }

    return std::memcmp(a.data(), b.data(), b.length()) == 0;
}

bool ends_with(std::string_view const& a, std::string_view const& b)
{
    if(b.length() > a.length())
    {
        return false;
    }

    return std::memcmp(
        a.data() + a.length() - b.length(),
        b.data(),
        b.length()
    ) == 0;
}

std::size_t count_occurrences(
        std::string_view const& a,
        std::string_view const& b)
{
    if(a.empty() || b.empty())
    {
        return 0;
    }

    std::size_t occurrences = 0;
    for(std::size_t i = 0; i < a.length() - (b.length() - 1);)
    {
        if(std::memcmp(a.data() + i, b.data(), b.length()) == 0)
        {
            ++occurrences;
            i += b.length();
        }
        else
        {
            ++i;
        }
    }

    return occurrences;
}

std::vector<std::string> split(
        std::string_view const& s,
        std::string_view const& delimiter)
{
    // get the number of occurrences of the character so we can reserve the
    // vector
    std::size_t occurrences = count_occurrences(s, delimiter);

    std::vector<std::string> ret;
    ret.reserve(occurrences + 1);

    std::size_t offset = 0;
    std::size_t i = 0;
    for(; i < s.length() - (delimiter.length() - 1);)
    {
        if(std::memcmp(s.data() + i, delimiter.data(), delimiter.length()) == 0)
        {
            ret.emplace_back(s.data() + offset, i - offset);
            i += delimiter.length();
            offset = i;
        }
        else
        {
            ++i;
        }
    }
    ret.emplace_back(s.data() + offset, i - offset);

    return ret;
}

std::string lstrip(
        std::string_view const& s,
        std::string_view const& chars)
{
    if(chars.empty())
    {
        return std::string(s);
    }

    std::size_t i = 0;
    for(; i < s.length(); ++i)
    {
        bool matched = false;
        for(std::size_t j = 0; j < chars.length(); ++j)
        {
            if(s[i] == chars[j])
            {
                matched = true;
                break;
            }
        }
        if(!matched)
        {
            break;
        }
    }
    return std::string(s.substr(i, s.length()));
}

std::string rstrip(
        std::string_view const& s,
        std::string_view const& chars)
{
    if(chars.empty())
    {
        return std::string(s);
    }

    int32_t i = static_cast<int>(s.length()) - 1;
    for(; i >= 0; --i)
    {
        bool matched = false;
        for(std::size_t j = 0; j < chars.length(); ++j)
        {
            if(s[i] == chars[j])
            {
                matched = true;
                break;
            }
        }
        if(!matched)
        {
            break;
        }
    }
    return std::string(s.substr(0, i + 1));
}

std::string strip(
        std::string_view const& s,
        std::string_view const& chars)
{
    return lstrip(rstrip(s, chars), chars);
}

std::string repeat(std::string const& s, std::size_t n)
{
    std::string ret;
    ret.reserve(s.length() * n);
    for(std::size_t i = 0; i < n; ++i)
    {
        ret += s;
    }
    return ret;
}

std::string capitalize(std::string_view const& s)
{
    std::string ret = std::string(s);
    if(!ret.empty())
    {
        ret[0] = toupper(ret[0]);
    }
    return ret;
}

} // namespace str
ARC_BASE_VERSION_NS_END
} // namespace arc
