/*!
 * \file
 * \author David Saxon
 * \brief Operations on strings.
 *
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ARCANECORE_BASE_STR_STRINGOPERATIONS_HPP_
#define ARCANECORE_BASE_STR_STRINGOPERATIONS_HPP_

#include <vector>

#include "arcanecore/base/BaseAPI.hpp"
#include "arcanecore/cxx17/string_view.hpp"


namespace arc
{
ARC_BASE_VERSION_NS_BEGIN
namespace str
{

/*!
 * \brief Returns whether the given string represents an unsigned integer in
 *        ASCII form. i.e. the string contains only digits.
 */
bool is_uint(std::string_view const& s);

/*!
 * \brief Returns whether the given string represents an integer in ASCII form.
 */
bool is_int(std::string_view const& s);

/*!
 * \brief Returns whether the given string represents a floating point number in
 *        ASCII, this supports scientific notation (e.g. 2.1684e-19).
 */
bool is_float(std::string_view const& s);

/*!
 * \brief Returns whether the given string is empty or consists of only
 *        whitespace characters.
 */
bool is_whitespace(std::string_view const& s);

/*!
 * \brief Checks whether string a starts with string b.
 */
bool starts_with(std::string_view const& a, std::string_view const& b);

/*!
 * \brief Checks whether string a ends with string b.
 */
bool ends_with(std::string_view const& a, std::string_view const& b);

/*!
 * \brief Returns the number of times string b occurs in string a without
 *        overlap.
 *
 * For example if string a is "banana" and string b is "ana", this function
 * would return 1 occurrence: "b|ana|na". If b was "an" this function would
 * return 2 occurrences: "b|an||an|a".
 */
std::size_t count_occurrences(
        std::string_view const& a,
        std::string_view const& b);

/*!
 * \brief Splits string s into 1 more substrings around occurrences of the
 *        delimiters string.
 */
std::vector<std::string> split(
        std::string_view const& s,
        std::string_view const& delimiter);

/*!
 * \brief Creates a new string which is the result of combining the strings
 *        defined by the iterators with a instance of the delimiter string
 *        between each pair.
 */
template<typename T_InputIterator>
std::string join(
        T_InputIterator const& first,
        T_InputIterator const& last,
        std::string_view const& delimiter)
{
    std::string ret;
    for(T_InputIterator it = first; it != last; ++it)
    {
        ret += *it;
        if(it + 1 != last)
        {
            ret += std::string(delimiter);
        }
    }
    return ret;
}

/*!
 * \brief Returns a copy of the given string with the characters defined by
 *        chars stripped from the left hand side of the string.
 */
std::string lstrip(
        std::string_view const& s,
        std::string_view const& chars = " \t\n\r");

/*!
 * \brief Returns a copy of the given string with the characters defined by
 *        chars stripped from the right hand side of the string.
 */
std::string rstrip(
        std::string_view const& s,
        std::string_view const& chars = " \t\n\r");

/*!
 * \brief Returns a copy of the given string with the characters defined by
 *        chars stripped from the left and right hand sides of the string.
 */
std::string strip(
        std::string_view const& s,
        std::string_view const& chars = " \t\n\r");

/*!
 * \brief Returns a new string that contains the input string repeated the given
 *        number of times.
 */
std::string repeat(std::string const& s, std::size_t n);

/*!
 * \brief Returns a copy of the given string with the first letter capitalized.
 */
std::string capitalize(std::string_view const& s);

} // namespace str
ARC_BASE_VERSION_NS_END
} // namespace arc

#endif
