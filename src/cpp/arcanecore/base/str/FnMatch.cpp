/*!
 * \file
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include "arcanecore/base/Preproc.hpp"
#include "arcanecore/base/str/FnMatch.hpp"

#include "arcanecore/base/Exceptions.hpp"
#include "arcanecore/base/str/StringOperations.hpp"

#ifdef ARC_OS_UNIX

    #include <fnmatch.h>

#elif defined(ARC_OS_WINDOWS)

    #include <windows.h>
    #include <Shlwapi.h>
    #pragma comment(lib, "Shlwapi.lib")

#endif


namespace arc
{
ARC_BASE_VERSION_NS_BEGIN
namespace str
{

bool fnmatch(std::string const& pattern, std::string const& s)
{
#ifdef ARC_OS_UNIX

    return ::fnmatch(pattern.c_str(), s.c_str(), 0) == 0;

#elif defined(ARC_OS_WINDOWS)

    return PathMatchSpecA(
        s.c_str(),
        pattern.c_str()
    );

#else

    throw arc::ex::NotImplementedError(
        "arc::str::fnmatch has not yet been implemented for this platform"
    );

#endif
}

} // namespace str
ARC_BASE_VERSION_NS_END
} // namespace arc
