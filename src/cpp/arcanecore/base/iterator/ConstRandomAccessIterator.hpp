/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2019, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ARCANECORE_BASE_ITERATOR_CONSTRANDOMACCESSITERATOR_HPP_
#define ARCANECORE_BASE_ITERATOR_CONSTRANDOMACCESSITERATOR_HPP_

#include <iterator>

#include "arcanecore/base/BaseAPI.hpp"


namespace arc
{
ARC_BASE_VERSION_NS_BEGIN
namespace iterator
{

/*!
 * \brief A random access iterator for a contiguous read-only data block.
 *
 * \tparam T_DataType The type of the data being iterated.
 */
template<typename T_DataType>
class ConstRandomAccessIterator
    : public std::iterator<std::random_access_iterator_tag, T_DataType>
{
public:

    //--------------------------------------------------------------------------
    //                                CONSTRUCTORS
    //--------------------------------------------------------------------------

    /*!
     * \brief Constructs a new ConstRandomAccessIterator that points to null
     *        data.
     */
    ConstRandomAccessIterator()
        : m_data_ptr(nullptr)
    {
    }

    /*!
     * \brief Constructs a new ConstRandomAccessIterator that point to the
     *        given data.
     */
    ConstRandomAccessIterator(const T_DataType* data_ptr)
        : m_data_ptr(data_ptr)
    {
    }

    /*!
     * \brief Copy constructor.
     */
    ConstRandomAccessIterator(const ConstRandomAccessIterator& other)
        : m_data_ptr(other.m_data_ptr)
    {
    }

    /*!
     * \brief Move constructor.
     */
    ConstRandomAccessIterator(ConstRandomAccessIterator&& other)
        : m_data_ptr(other.m_data_ptr)
    {
        other.m_data_ptr = nullptr;
    }

    //--------------------------------------------------------------------------
    //                                 DESTRUCTOR
    //--------------------------------------------------------------------------

    virtual ~ConstRandomAccessIterator()
    {
    }

    //--------------------------------------------------------------------------
    //                                 OPERATORS
    //--------------------------------------------------------------------------

    /*!
     * \brief Assigns the data being iterated.
     */
    ConstRandomAccessIterator& operator=(const T_DataType* data_ptr)
    {
        m_data_ptr = data_ptr;
        return *this;
    }

    /*!
     * \brief Copy assignment.
     */
    ConstRandomAccessIterator& operator=(const ConstRandomAccessIterator& other)
    {
        m_data_ptr = other.m_data_ptr;
        return *this;
    }

    /*!
     * \brief Move assignment.
     */
    ConstRandomAccessIterator& operator=(ConstRandomAccessIterator&& other)
    {
        m_data_ptr = other.m_data_ptr;
        other.m_data_ptr = nullptr;
        return *this;
    }

    /*!
     * \brief Equality operator.
     */
    bool operator==(const ConstRandomAccessIterator& other) const
    {
        return m_data_ptr == other.m_data_ptr;
    }

    /*!
     * \brief Inequality operator.
     */
    bool operator!=(const ConstRandomAccessIterator& other) const
    {
        return m_data_ptr != other.m_data_ptr;
    }

    /*!
     * \brief Greater than operator.
     */
    bool operator>(const ConstRandomAccessIterator& other) const
    {
        return m_data_ptr > other.m_data_ptr;
    }

    /*!
     * \brief Greater than or equal to operator.
     */
    bool operator>=(const ConstRandomAccessIterator& other) const
    {
        return m_data_ptr >= other.m_data_ptr;
    }

    /*!
     * \brief Less than operator.
     */
    bool operator<(const ConstRandomAccessIterator& other) const
    {
        return m_data_ptr < other.m_data_ptr;
    }

    /*!
     * \brief Less than or equal to operator.
     */
    bool operator<=(const ConstRandomAccessIterator& other) const
    {
        return m_data_ptr <= other.m_data_ptr;
    }

    /*!
     * \brief Returns the underlying data value this iterator points to.
     */
    const T_DataType& operator*() const
    {
        return *m_data_ptr;
    }

    /*!
     * \brief Returns the underlying data value this iterator points to.
     */
    const T_DataType& operator->() const
    {
        return *m_data_ptr;
    }

    /*!
     * \brief Returns a new iterator that is moved forward from this iterator by
     *        given amount.
     */
    ConstRandomAccessIterator operator+(int amount) const
    {
        return ConstRandomAccessIterator(m_data_ptr + amount);
    }

    /*!
     * \brief Moves this iterator forward by the given amount.
     */
    ConstRandomAccessIterator& operator+=(int amount)
    {
        m_data_ptr += amount;
    }

    /*!
     * \brief Moves this iterator forward by one position.
     */
    ConstRandomAccessIterator& operator++()
    {
        ++m_data_ptr;
        return *this;
    }

    /*!
     * \brief Returns a new iterator at the current position and moves this
     *        iterator forward by one position.
     */
    ConstRandomAccessIterator operator++(int amount)
    {
        ConstRandomAccessIterator temp(*this);
        ++m_data_ptr;
        return temp;
    }

    /*!
     * \brief Returns a new iterator that is moved backward from this iterator
     *        by given amount.
     */
    ConstRandomAccessIterator operator-(int amount) const
    {
        return ConstRandomAccessIterator(m_data_ptr - amount);
    }

    /*!
     * \brief Moves this iterator backward by the given amount.
     */
    ConstRandomAccessIterator& operator-=(int amount)
    {
        m_data_ptr -= amount;
    }

    /*!
     * \brief Moves this iterator backward by one position.
     */
    ConstRandomAccessIterator& operator--()
    {
        --m_data_ptr;
        return *this;
    }

    /*!
     * \brief Returns a new iterator at the current position and moves this
     *        iterator backward by one position.
     */
    ConstRandomAccessIterator operator--(int amount)
    {
        ConstRandomAccessIterator temp(*this);
        --m_data_ptr;
        return temp;
    }

private:

    //--------------------------------------------------------------------------
    //                             PRIVATE ATTRIBUTES
    //--------------------------------------------------------------------------

    /*!
     * \brief Pointer to the data being iterated.
     */
    const T_DataType* m_data_ptr;
};

} // namespace iterator
ARC_BASE_VERSION_NS_END
} // namespace arc

#endif
