/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <limits>

#include "arcanecore/base/data/BitwiseFloat.hpp"


namespace arc
{
ARC_BASE_VERSION_NS_BEGIN
namespace data
{

//------------------------------------------------------------------------------
//                                  CONSTRUCTORS
//------------------------------------------------------------------------------

BitwiseFloat::BitwiseFloat(float value)
    : float_rep(value)
{
}

BitwiseFloat::BitwiseFloat(uint32_t value)
    : int_rep(value)
{
}

BitwiseFloat::BitwiseFloat(BitwiseFloat const& other)
    : float_rep(other.float_rep)
{
}

BitwiseFloat::BitwiseFloat(BitwiseFloat&& other)
    : float_rep(other.float_rep)
{
}

//------------------------------------------------------------------------------
//                                   OPERATORS
//------------------------------------------------------------------------------

BitwiseFloat& BitwiseFloat::operator=(BitwiseFloat const& other)
{
    float_rep = other.float_rep;
    return *this;
}

BitwiseFloat& BitwiseFloat::operator=(BitwiseFloat&& other)
{
    float_rep = other.float_rep;
    return *this;
}

bool BitwiseFloat::operator==(BitwiseFloat const& other) const
{
    return int_rep == other.int_rep;
}

bool BitwiseFloat::operator!=(BitwiseFloat const& other) const
{
    return !((*this) == other);
}

//------------------------------------------------------------------------------
//                            PUBLIC MEMBER FUNCTIONS
//------------------------------------------------------------------------------

bool BitwiseFloat::get_sign_bit() const
{
    return (int_rep >> 31) == 1;
}

void BitwiseFloat::set_sign_bit(bool sign)
{
    int_rep =
        (static_cast<int32_t>(sign) << 31) | (int_rep & 0x7FFFFFFF);
}

uint32_t BitwiseFloat::get_exponent() const
{
    return (int_rep >> 23) & 0xFF;
}

void BitwiseFloat::set_exponent(uint32_t exponent)
{
    int_rep =
        (static_cast<uint32_t>(exponent) << 23) | (int_rep & 0x807FFFFF);
}

uint32_t BitwiseFloat::get_mantissa() const
{
    return int_rep & 0x7FFFFF;
}

void BitwiseFloat::set_mantissa(uint32_t mantissa)
{
    int_rep = (mantissa & 0x7FFFFF) | (int_rep & 0xFF800000);
}

float BitwiseFloat::precision_away_from_zero() const
{
    // can't check precision for infinity or NaN
    if(get_exponent() == 255)
    {
        return std::numeric_limits<float>::infinity();
    }

    // create a copy and increment
    BitwiseFloat c(float_rep);
    ++c.int_rep;
    // return delta
    return c.float_rep - float_rep;
}

float BitwiseFloat::precision_towards_zero() const
{
    // already at 0?
    if (get_exponent() == 0 && get_mantissa() == 0)
    {
        return 0.0F;
    }
    // NaN?
    if (get_exponent() == 255 && get_mantissa() != 0)
    {
        return std::numeric_limits<float>::infinity();
    }

    // create a copy and decrement
    BitwiseFloat c(float_rep);
    --c.int_rep;
    // return delta
    return -(c.float_rep - float_rep);
}

} // namespace data
ARC_BASE_VERSION_NS_END
} // namespace arc
