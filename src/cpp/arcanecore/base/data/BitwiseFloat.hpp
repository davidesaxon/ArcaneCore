/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ARCANECORE_BASE_DATA_BITWISEFLOAT_HPP_
#define ARCANECORE_BASE_DATA_BITWISEFLOAT_HPP_

#include <cstdint>

#include "arcanecore/base/BaseAPI.hpp"


namespace arc
{
ARC_BASE_VERSION_NS_BEGIN
namespace data
{

/*!
 * \brief Object that can be used to read and write the bits of a floating point
 *        number.
 *
 * Floating point numbers are composed of three sections: sign, exponent, and
 * mantissa. These sections are laid out like so:
 *
 * \code
 * 0 00000000 00000000000000000000000
 * ^ \------/ \---------------------/
 * |     |                |
 * \ 8-bit exponent       |
 *  \                23-bit mantissa
 * sign bit
 * \endcode
 *
 * This object provides functions for reading and writing these sections
 * separately.
 */
union BitwiseFloat
{
public:

    //--------------------------------------------------------------------------
    //                             PUBLIC ATTRIBUTES
    //--------------------------------------------------------------------------

    /*!
     * \brief Access to the float representation.
     */
    float float_rep;
    /*!
     * \brief Access to the int representation.
     */
    uint32_t int_rep;

    //--------------------------------------------------------------------------
    //                                CONSTRUCTORS
    //--------------------------------------------------------------------------

    /*!
     * \brief Float constructor.
     *
     * Creates a new BitewiseFloat initialized with the given floating point
     * value.
     */
    explicit BitwiseFloat(float value);

    /*!
     * \brief Int constructor.
     *
     * Creates a new BitwiseFloat initialized with the bytes of the given
     * integer value.
     */
    explicit BitwiseFloat(uint32_t value);

    /*!
     * \brief Copy constructor.
     */
    BitwiseFloat(BitwiseFloat const& other);

    /*!
     * \brief Move constructor.
     */
    BitwiseFloat(BitwiseFloat&& other);

    //--------------------------------------------------------------------------
    //                                 OPERATORS
    //--------------------------------------------------------------------------

    /*!
     * \brief Copy assignment operator.
     */
    BitwiseFloat& operator=(BitwiseFloat const& other);

    /*!
     * \brief Copy assignment operator.
     */
    BitwiseFloat& operator=(BitwiseFloat&& other);

    /*!
     * \brief Equality operator.
     *
     * Equality is defined by a comparison of the exact BitwiseFloat::int_rep
     * values.
     *
     * For more flexible floating point comparisons see
     * arc::data::float_equals().
     */
    bool operator==(BitwiseFloat const& other) const;

    /*!
     * \brief Inequality operator.
     *
     * Equality is defined by a comparison of the exact BitwiseFloat::int_rep
     * values.
     *
     * For more flexible floating point comparisons see
     * arc::data::float_equals().
     */
    bool operator!=(BitwiseFloat const& other) const;

    //--------------------------------------------------------------------------
    //                          PUBLIC MEMBER FUNCTIONS
    //--------------------------------------------------------------------------

    /*!
     * \brief Retrieves the sign bit of this floating point number.
     */
    bool get_sign_bit() const;

    /*!
     * \brief Sets the sign bit (most significant bit) of this float.
     */
    void set_sign_bit(bool sign);

    /*!
     * \brief Retrieves the 8-bit exponent section of this floating point
     *        number.
     */
    uint32_t get_exponent() const;

    /*!
     * \brief Sets the 8-bit exponent section of this floating point number.
     *
     * \note While a 32-bit input value is accepted only the least significant
     *       8 bits of this value will be used.
     */
    void set_exponent(uint32_t exponent);

    /*!
     * \brief Retrieves the 23-bit mantissa section of this floating point
     *        number.
     */
    uint32_t get_mantissa() const;

    /*!
     * \brief Sets the 23-bit mantissa section of this floating point number.
     *
     * \note While a 32-bit input value is accepted only the least significant
     *       23 bits of this value will be used.
     */
    void set_mantissa(uint32_t mantissa);

    /*!
     * \brief Returns the precision of this float away from zero.
     *
     * Floating point precision is measured as the difference between this
     * number and the next possible float value away from zero. Returns infinity
     * if this float is infinity or NaN.
     */
    float precision_away_from_zero() const;

    /*!
     * \brief Returns the precision of this float away from zero.
     *
     * Floating point precision is measured as the difference between this
     * number and the next possible float value towards zero. Returns 0 if this
     * float is already 0 and infinity if this float is NaN.
     */
    float precision_towards_zero() const;
};

} // namespace data
ARC_BASE_VERSION_NS_END
} // namespace arc

#endif
