/*!
 * \file
 * \author David Saxon
 * \brief Extended functionality for floating point numbers.
 *
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ARCANECORE_BASE_DATA_FLOATOPERATIONS_HPP_
#define ARCANECORE_BASE_DATA_FLOATOPERATIONS_HPP_

#include <limits>

#include "arcanecore/base/BaseAPI.hpp"


namespace arc
{
ARC_BASE_VERSION_NS_BEGIN
namespace data
{

/*!
 * \brief Checks whether two floating point values are equal or almost equal.
 *
 * This performs a two stage check:
 *
 * - First the values are subtracted from one another and if the absolute result
 *   is less than or equal to ```delta_threshold``` then this operation resolves
 *   true.
 *
 * - Second the integer representations of the values are subtracted from one
 *   another and if the absolute result is less than or equal to
 *   ```ulps_threshold``` then this function resolves true.
 *
 * The first check is performed for the purpose of values near 0 where ULPs
 * style comparisons break down, e.g. ```0.0F == -0.0F```.
 *
 * The second comparison is a comparison of the difference of units in last
 * place (ULPs) between the two floats. This effectively compares the number
 * of possible float representations between the two values. Larger numbers with
 * less precision will have a bigger numerical difference than two smaller
 * values with more precision for the same ULPs difference.
 *
 * \param a The first float to compare.
 * \param b The second float to compare.
 * \param delta_threshold If ```a``` and ```b``` have a difference less than
 *                        or equal to this, they are considered equal.
 * \param ulps_threshold If ```a``` and ```b``` have units in last place
 *                       difference less than or equal to this, they are
 *                       considered equal.
 */
bool float_equals(
        float a,
        float b,
        float delta_threshold = std::numeric_limits<float>::epsilon(),
        uint32_t ulps_threshold = 2);

} // namespace data
ARC_BASE_VERSION_NS_END
} // namespace arc

#endif
