/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <cmath>

#include "arcanecore/base/data/BitwiseFloat.hpp"
#include "arcanecore/base/data/FloatOperations.hpp"


namespace arc
{
ARC_BASE_VERSION_NS_BEGIN
namespace data
{

bool float_equals(
        float a,
        float b,
        float delta_threshold,
        uint32_t ulps_threshold)
{
    // first perform the delta threshold check, this is needed for values near 0
    if(std::abs(a - b) <= delta_threshold)
    {
        return true;
    }

    // TODO: could optimise here

    // create bitwise representations of the floats
    arc::data::BitwiseFloat b_a(a);
    arc::data::BitwiseFloat b_b(b);

    // ulps comparisons only work on values of the same sign
    if(b_a.get_sign_bit() != b_b.get_sign_bit())
    {
        return false;
    }

    // find the difference in ulps
    int64_t ulps = std::abs(
        static_cast<int64_t>(b_a.int_rep) - static_cast<int64_t>(b_b.int_rep)
    );

    return ulps <= static_cast<int64_t>(ulps_threshold);
}

} // namespace data
ARC_BASE_VERSION_NS_END
} // namespace arc
