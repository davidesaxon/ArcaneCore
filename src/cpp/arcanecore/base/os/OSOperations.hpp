/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ARCANECORE_BASE_OS_OSOPERATIONS_HPP_
#define ARCANECORE_BASE_OS_OSOPERATIONS_HPP_

#include <string>

#include "arcanecore/base/BaseAPI.hpp"


namespace arc
{
ARC_BASE_VERSION_NS_BEGIN
namespace os
{

/*!
 * \brief Gets the last system error message.
 *
 * This should be used after platform specific call that has failed.
 */
std::string get_last_system_error_message();

/*!
 * \brief Returns the name of this operating system.
 */
 std::string const& get_os_name();

/*!
 * \brief Returns the name/version of this operating system's distribution.
 */
 std::string const& get_distro_name();

/*!
 * \brief Returns the vendor of this system's CPU.
 */
 std::string const& get_cpu_vendor();

/*!
 * \brief Returns the model of this system's CPU.
 */
std::string const& get_cpu_model();

/*!
 * \brief Returns the number of physical cores this machine has.
 */
std::size_t get_cpu_physical_cores();

/*!
 * \brief Returns the number of logical processing units this machine has.
 */
std::size_t get_cpu_logical_processors();

/*!
 * \brief Get the clock rate (in MHz) of this machine's cpu.
 */
float get_cpu_clock_rate();

/*!
 * \brief Get the logical processor this thread is executing on.
 */
std::size_t get_thread_processor();

/*!
 * \brief Returns the total amount of main RAM on this system, measured in
 *        bytes.
 */
std::size_t get_total_ram();

/*!
 * \brief Returns the amount of RAM that is currently free, measured in bytes.
 */
std::size_t get_free_ram();

/*!
 * \brief Returns the total amount of virtual memory on this system, measured
 *        in bytes.
 */
std::size_t get_total_virtual_memory();

/*!
 * \brief Returns the amount of virtual memory that is currently free, measured
 *        in bytes.
 */
std::size_t get_free_virtual_memory();

/*!
 * \brief Returns the current resident set size (physical memory use) measured
 *        in bytes.
 */
std::size_t get_rss();

/*!
 * \brief Returns the peak (maximum so far) resident set size (physical memory
 *        use) measured in bytes.
 */
std::size_t get_peak_rss();

} // namespace os
ARC_BASE_VERSION_NS_END
} // namespace arc

#endif
