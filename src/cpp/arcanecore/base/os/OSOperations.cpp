/*!
 * \file
 * \author David Saxon
 *
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#include <arcanecore/base/Exceptions.hpp>
#include "arcanecore/base/Preproc.hpp"
#include "arcanecore/base/str/StringOperations.hpp"

#ifdef ARC_OS_UNIX

    #include <cstring>
    #include <cerrno>
    #include <fstream>
    #include <sched.h>
    #include <unistd.h>
    #include <sys/resource.h>
    #include "sys/sysinfo.h"
    #include "sys/types.h"

#elif defined(ARC_OS_WINDOWS)

    #include <windows.h>
    #include <intrin.h>
    #include <psapi.h>
    #include <VersionHelpers.h>

    #pragma comment(lib, "psapi.lib")

#endif

#include "arcanecore/base/Exceptions.hpp"
#include "arcanecore/base/os/OSOperations.hpp"
#include "arcanecore/base/str/StringOperations.hpp"


namespace arc
{
ARC_BASE_VERSION_NS_BEGIN
namespace os
{

//------------------------------------------------------------------------------
//                                    GLOBALS
//------------------------------------------------------------------------------

namespace
{

// stores infomation about the CPU
static std::string g_cpu_vendor;
static std::string g_cpu_model;
static std::size_t g_cpu_phyiscal_cores     = 0;
static std::size_t g_cpu_logical_processors = 0;
static float g_cpu_clock_rate = 0.0F;

} // namespace anonymous

//------------------------------------------------------------------------------
//                                   FUNCTIONS
//------------------------------------------------------------------------------

std::string get_last_system_error_message()
{
    #ifdef ARC_OS_UNIX

        return std::string(strerror(errno));

    #elif defined(ARC_OS_WINDOWS)

        // TODO: add support Unicode errors
        // LPWSTR error_message = nullptr;
        // std::size_t message_size = FormatMessageW(
        //     FORMAT_MESSAGE_ALLOCATE_BUFFER |
        //         FORMAT_MESSAGE_FROM_SYSTEM     |
        //         FORMAT_MESSAGE_IGNORE_INSERTS,
        //     NULL,
        //     GetLastError(),
        //     MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        //     (LPWSTR) &error_message,
        //     0,
        //     NULL
        // );

        LPTSTR error_message = nullptr;
        std::size_t message_size = FormatMessage(
            FORMAT_MESSAGE_ALLOCATE_BUFFER     |
                FORMAT_MESSAGE_FROM_SYSTEM     |
                FORMAT_MESSAGE_IGNORE_INSERTS,
            NULL,
            GetLastError(),
            MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
            (LPTSTR) &error_message,
            0,
            NULL
        );
        // copy to std string
        std::string ret(error_message);
        // destroy the system string
        LocalFree(error_message);

        // remove a newline from the end of the message if it exists
        if(arc::str::ends_with(ret, "\n"))
        {
            ret = ret.substr(0, ret.length() - 1);
        }
        // remove a carriage return from the end of the message if it exists
        if(arc::str::ends_with(ret, "\r"))
        {
            ret = ret.substr(0, ret.length() - 1);
        }
        return ret;

    #else

        throw arc::ex::NotImplementedError(
            "arc::os::get_last_system_error_message has not yet been "
            "implemented for this platform"
        );

    #endif
}

std::string const& get_os_name()
{
    #ifdef ARC_OS_WINDOWS

        static std::string os_name("Windows");

    #elif defined(ARC_OS_LINUX)

        static std::string os_name("Linux");

    #elif defined(ARC_OS_MAC)

        static std::string os_name("Mac");

    #elif defined(ARC_OS_UNIX)

        static std::string os_name("UNKNOWN-UNIX");

    #else

        static std::string os_name("UNKNOWN");

    #endif

    return os_name;
}

std::string const& get_distro_name()
{
    static std::string distro_name;
    if(distro_name.empty())
    {
        #ifdef ARC_OS_WINDOWS

            std::string windows_type = "Client";
            if(IsWindowsServer())
            {
                windows_type = "Server";
            }
            windows_type = "Windows " + windows_type;

            typedef LONG NTSTATUS, *PNTSTATUS;
            typedef NTSTATUS (WINAPI* RtlGetVersionPtr)(PRTL_OSVERSIONINFOW);

            HMODULE hMod = GetModuleHandleW(L"ntdll.dll");
            if(hMod)
            {
                RtlGetVersionPtr fx_ptr =
                    (RtlGetVersionPtr) GetProcAddress(hMod, "RtlGetVersion");
                if(fx_ptr != nullptr)
                {
                    RTL_OSVERSIONINFOW rovi = {0};
                    rovi.dwOSVersionInfoSize = sizeof(rovi);
                    if(fx_ptr(&rovi) == 0)
                    {
                        distro_name +=
                            std::to_string(rovi.dwMajorVersion) + "." +
                            std::to_string(rovi.dwMinorVersion);
                    }
                }
            }

            distro_name = windows_type + " " + distro_name;

        #elif defined(ARC_OS_LINUX)

            std::ifstream distro_file("/etc/lsb-release");
            if(!distro_file.good())
            {
                throw arc::ex::IOError(
                    "Failed to open system file \"/etc/lsb-release\""
                );
            }

            std::string distro_id = "UNKNOWN";
            std::string const distro_id_key = "DISTRIB_ID=";
            std::string distro_version;
            std::string const distro_version_key = "DISTRIB_RELEASE=";

            std::string line;
            while(std::getline(distro_file, line))
            {
                if(arc::str::starts_with(line, distro_id_key))
                {
                    distro_id = line.substr(
                        distro_id_key.length(),
                        line.length()
                    );
                }
                else if(arc::str::starts_with(line, distro_version_key))
                {
                    distro_version = line.substr(
                        distro_version_key.length(),
                        line.length()
                    );
                }
            }

            distro_name = distro_id + "-" + distro_version;
        #else

            throw arc::ex::NotImplementedError(
                "get_distro_name has not been implemented for this platform."
            );

        #endif
    }
    return distro_name;
}

// discovers and stores information about this system's CPU - but only does this
// once.
static void discover_cpu_info()
{
    static bool discovery_done = false;
    if(discovery_done)
    {
        return;
    }

    #ifdef ARC_OS_WINDOWS

        // get vendor
        //----------------------------------------------------------------------
        int32_t vendor_regs[4] = {0};
        char c_vendor[13];
        __cpuid(vendor_regs, 0);
        memcpy(c_vendor + 0, &vendor_regs[1], 4);
        memcpy(c_vendor + 4, &vendor_regs[3], 4);
        memcpy(c_vendor + 8, &vendor_regs[2], 4);
        c_vendor[12] = '\0';
        g_cpu_vendor = c_vendor;

        // get model
        //----------------------------------------------------------------------
        int32_t model_regs[4] = {0};
        char c_model[64];
        __cpuid(model_regs, 0x80000000);

        uint32_t n = model_regs[0];
        for(uint32_t i = 0x80000000; i  < n; ++i)
        {
            __cpuid(model_regs, i);
            if(i == 0x80000002)
            {
                memcpy(c_model, model_regs, sizeof(model_regs));
            }
            else if(i == 0x80000003)
            {
                memcpy(c_model + 16, model_regs, sizeof(model_regs));
            }
            else if(i == 0x80000004)
            {
                memcpy(c_model + 32, model_regs, sizeof(model_regs));
            }
        }
        g_cpu_model = c_model;

        // get number of physical cores
        //----------------------------------------------------------------------
        typedef BOOL (WINAPI *LPFN_GLPI)(
                PSYSTEM_LOGICAL_PROCESSOR_INFORMATION,
                PDWORD
        );

        LPFN_GLPI glpi = (LPFN_GLPI) GetProcAddress(
            GetModuleHandle(TEXT("kernel32")),
            "GetLogicalProcessorInformation"
        );
        if(glpi != nullptr)
        {
            PSYSTEM_LOGICAL_PROCESSOR_INFORMATION buffer = nullptr;
            DWORD return_length = 0;
            bool done = false;
            bool failure = false;
            while(!done)
            {
                DWORD rc = glpi(buffer, &return_length);

                if(rc == FALSE)
                {
                    if(GetLastError() == ERROR_INSUFFICIENT_BUFFER)
                    {
                        if(buffer != nullptr)
                        {
                            free(buffer);
                        }

                        buffer = (PSYSTEM_LOGICAL_PROCESSOR_INFORMATION)
                            malloc(return_length);
                        if(buffer == nullptr)
                        {
                            failure = true;
                            break;
                        }
                    }
                    else
                    {
                        failure = true;
                        break;
                    }
                }
                else
                {
                    done = true;
                }
            }

            PSYSTEM_LOGICAL_PROCESSOR_INFORMATION ptr = buffer;
            DWORD byte_offset = 0;
            while(sizeof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION) + byte_offset <=
                  return_length && !failure)
            {
                if(ptr->Relationship == RelationProcessorCore)
                {
                    ++g_cpu_phyiscal_cores;
                }

                byte_offset += sizeof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION);
                ++ptr;
            }
        }

        // get number of logical processors
        //----------------------------------------------------------------------
        SYSTEM_INFO sysinfo;
        GetSystemInfo(&sysinfo);
        g_cpu_logical_processors = sysinfo.dwNumberOfProcessors;

        // get clock rate
        //----------------------------------------------------------------------
        HKEY h_key;
        long l_error = RegOpenKeyEx(
            HKEY_LOCAL_MACHINE,
            "HARDWARE\\DESCRIPTION\\System\\CentralProcessor\\0",
            0,
            KEY_READ,
            &h_key
        );
        if(l_error == ERROR_SUCCESS)
        {
            DWORD dw_mhz = _MAX_PATH;
            DWORD buffer_size = _MAX_PATH;
            RegQueryValueEx(
                h_key,
                "~MHz",
                nullptr,
                nullptr,
                (LPBYTE) &dw_mhz,
                &buffer_size
            );
            g_cpu_clock_rate = static_cast<float>(dw_mhz);
        }

    #elif defined(ARC_OS_LINUX)

        // use ifsteam since this is special system file
        std::ifstream info_file("/proc/cpuinfo");
        if(!info_file.good())
        {
            throw arc::ex::IOError(
                "Failed to open system file \"/proc/cpuinfo\""
            );
        }

        // various keys within the file
        std::string const vendor_key = "vendor_id\t: ";
        std::string const model_key = "model name\t: ";
        std::string const physical_cores_key = "cpu cores\t: ";
        std::string const logical_processors_key = "processor\t: ";
        std::string const clock_rate_key = "cpu MHz\t\t: ";

        std::string line;
        while (std::getline(info_file, line))
        {
            if(g_cpu_vendor.empty() &&
               arc::str::starts_with(line, vendor_key))
            {
                g_cpu_vendor = line.substr(vendor_key.length(), line.length());
            }
            else if(g_cpu_model.empty() &&
                    arc::str::starts_with(line, model_key))
            {
                g_cpu_model = line.substr(model_key.length(), line.length());
            }
            else if(g_cpu_phyiscal_cores == 0 &&
                    arc::str::starts_with(line, physical_cores_key))
            {
                g_cpu_phyiscal_cores = std::stoul(line.substr(
                    physical_cores_key.length(),
                    line.length()
                ));
            }
            else if(arc::str::starts_with(line, logical_processors_key))
            {
                ++g_cpu_logical_processors;
            }
            else if(g_cpu_clock_rate <= 0.001F &&
                    arc::str::starts_with(line, clock_rate_key))
            {
                g_cpu_clock_rate = std::stof(line.substr(
                    clock_rate_key.length(),
                    line.length()
                ));
            }
        }

    #else

        throw arc::ex::NotImplementedError(
            "discover_cpu_info has not been implemented for this platform."
        );

    #endif

    discovery_done = true;
}

std::string const& get_cpu_vendor()
{
    discover_cpu_info();

    return g_cpu_vendor;
}

std::string const& get_cpu_model()
{
    discover_cpu_info();

    return g_cpu_model;
}

std::size_t get_cpu_physical_cores()
{
    discover_cpu_info();

    return g_cpu_phyiscal_cores;
}

std::size_t get_cpu_logical_processors()
{
    discover_cpu_info();

    return g_cpu_logical_processors;
}

float get_cpu_clock_rate()
{
    discover_cpu_info();

    return g_cpu_clock_rate;
}

std::size_t get_thread_processor()
{
    #ifdef ARC_OS_WINDOWS

        return static_cast<std::size_t>(GetCurrentProcessorNumber());

    #elif defined(ARC_OS_LINUX)

        return static_cast<std::size_t>(sched_getcpu());

    #else

        throw arc::ex::NotImplementedError(
            "get_thread_processor has not been implemented for this platform."
        );

    #endif
}

std::size_t get_total_ram()
{
    #ifdef ARC_OS_WINDOWS

        MEMORYSTATUSEX statex;
        statex.dwLength = sizeof(statex);
        GlobalMemoryStatusEx(&statex);
        return static_cast<std::size_t>(statex.ullTotalPhys);

    #elif defined(ARC_OS_LINUX)

        struct sysinfo mem_info;
        sysinfo(&mem_info);
        return static_cast<std::size_t>(mem_info.totalram);

    #else

        throw arc::ex::NotImplementedError(
            "get_total_ram has not been implemented for this platform."
        );

    #endif
}

std::size_t get_free_ram()
{
    #ifdef ARC_OS_WINDOWS

        MEMORYSTATUSEX statex;
        statex.dwLength = sizeof(statex);
        GlobalMemoryStatusEx(&statex);
        return static_cast<std::size_t>(statex.ullAvailPhys);

    #elif defined(ARC_OS_LINUX)

        struct sysinfo mem_info;
        sysinfo(&mem_info);
        return static_cast<std::size_t>(mem_info.freeram);

    #else

        throw arc::ex::NotImplementedError(
            "get_free_ram has not been implemented for this platform."
        );

    #endif
}

std::size_t get_total_virtual_memory()
{
    #ifdef ARC_OS_WINDOWS

        MEMORYSTATUSEX statex;
        statex.dwLength = sizeof(statex);
        GlobalMemoryStatusEx(&statex);
        return static_cast<std::size_t>(statex.ullTotalVirtual);

    #elif defined(ARC_OS_LINUX)

        struct sysinfo mem_info;
        sysinfo(&mem_info);
        return static_cast<std::size_t>(mem_info.totalswap);

    #else

        throw arc::ex::NotImplementedError(
            "get_total_virtual_memory has not been implemented for this "
            "platform."
        );

    #endif
}

std::size_t get_free_virtual_memory()
{
    #ifdef ARC_OS_WINDOWS

        MEMORYSTATUSEX statex;
        statex.dwLength = sizeof(statex);
        GlobalMemoryStatusEx(&statex);
        return static_cast<std::size_t>(statex.ullAvailVirtual);

    #elif defined(ARC_OS_LINUX)

        struct sysinfo mem_info;
        sysinfo(&mem_info);
        return static_cast<std::size_t>(mem_info.freeswap);

    #else

        throw arc::ex::NotImplementedError(
            "get_free_virtual_memory has not been implemented for this "
            "platform."
        );

    #endif
}

std::size_t get_rss()
{
    #ifdef ARC_OS_WINDOWS

        PROCESS_MEMORY_COUNTERS info;
        GetProcessMemoryInfo(GetCurrentProcess(), &info, sizeof(info));
        return static_cast<size_t>(info.WorkingSetSize);

    #elif defined(ARC_OS_LINUX)

        long rss = 0L;
        FILE* file = nullptr;
        // open the file
        if((file = fopen("/proc/self/statm", "r")) == nullptr)
        {
            throw arc::ex::IOError(
                "Failed to open \"/proc/self/statm\" to read RSS"
            );
        }
        // read from the file
        if(fscanf(file, "%*s%ld", &rss) != 1)
        {
            fclose(file);
            throw arc::ex::ParseError(
                "Failed to parse \"/proc/self/statm\" to read RSS"
            );
        }
        fclose(file);
        return
            static_cast<size_t>(rss) *
            static_cast<size_t>(sysconf( _SC_PAGESIZE));

    #elif defined(ARC_OS_MAC)

        struct mach_task_basic_info info;
        mach_msg_type_number_t infoCount = MACH_TASK_BASIC_INFO_COUNT;
        if(task_info(
                mach_task_self(),
                MACH_TASK_BASIC_INFO,
                (task_info_t) &info,
                &infoCount
            ) != KERN_SUCCESS
        )
        {
            return 0;
        }
        return static_cast<size_t>(info.resident_size);

    #else

        throw arc::ex::NotImplementedError(
            "get_rss has not been implemented for this platform."
        );

    #endif
}

std::size_t get_peak_rss()
{
    #ifdef ARC_OS_WINDOWS

        PROCESS_MEMORY_COUNTERS info;
        GetProcessMemoryInfo(GetCurrentProcess(), &info, sizeof(info));
        return static_cast<size_t>(info.PeakWorkingSetSize);

    #elif defined(ARC_OS_LINUX)

        struct rusage rusage;
        getrusage(RUSAGE_SELF, &rusage);
        return static_cast<size_t>(rusage.ru_maxrss * 1024L);

    #elif defined(ARC_OS_MAC)

        struct rusage rusage;
        getrusage(RUSAGE_SELF, &rusage);
        return static_cast<size_t>(rusage.ru_maxrss);

    #else

        throw arc::ex::NotImplementedError(
            "get_peak_rss has not been implemented for this platform."
        );

    #endif
}

} // namespace os
ARC_BASE_VERSION_NS_END
} // namespace arc
