/*!
 * \file
 * \author David Saxon
 * \brief Functions relating to math on 4x4 float matrices.
 *
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ARCANECORE_LX_MATRIXMATH44F_HPP_
#define ARCANECORE_LX_MATRIXMATH44F_HPP_

#include <cstdint>

#include <arcanecore/base/math/MathOperations.hpp>

#include "arcanecore/lx/AxisAngle.hpp"
#include "arcanecore/lx/LXAPI.hpp"
#include "arcanecore/lx/Matrix.hpp"
#include "arcanecore/lx/Quaternion.hpp"
#include "arcanecore/lx/Vector.hpp"
#include "arcanecore/lx/__eigen/Dense"


namespace arclx
{
ARCLX_VERSION_NS_BEGIN

/*!
 * \brief Defines the possible orders in which rotations can be applied when
 *        computing a transformation matrix.
 */
enum class RotationOrder
{
    kXYZ,
    kXZY,
    kYXZ,
    kYZX,
    kZXY,
    kZYX
};

/*!
 * \brief Produces a 4x4 affine matrix representing the given 3-dimensional
 *        translation.
 */
inline arclx::Matrix44f translate_44f(arclx::Vector3f const& translation)
{
    arclx::Matrix44f result;
    result
        << 1.0F, 0.0F, 0.0F, translation(0),
           0.0F, 1.0F, 0.0F, translation(1),
           0.0F, 0.0F, 1.0F, translation(2),
           0.0F, 0.0F, 0.0F, 1.0F;
    return result;
}

/*!
 * \brief Produces a 4x4 affine matrix representing the given euler angle (in
 *        radians).
 */
inline arclx::Matrix44f rotate_euler_44f(
        arclx::Vector3f const& euler,
        RotationOrder order = RotationOrder::kZYX)
{
    arclx::Matrix44f result = arclx::Matrix44f::Identity();
    arclx::Quaternionf quat_rot;
    switch(order)
    {
        case RotationOrder::kXYZ:
        {
            quat_rot =
                arclx::AxisAnglef(euler(0), arclx::Vector3f::UnitX()) *
                arclx::AxisAnglef(euler(1), arclx::Vector3f::UnitY()) *
                arclx::AxisAnglef(euler(2), arclx::Vector3f::UnitZ());
            break;
        }
        case RotationOrder::kXZY:
        {
            quat_rot =
                arclx::AxisAnglef(euler(0), arclx::Vector3f::UnitX()) *
                arclx::AxisAnglef(euler(2), arclx::Vector3f::UnitZ()) *
                arclx::AxisAnglef(euler(1), arclx::Vector3f::UnitY());
            break;
        }
        case RotationOrder::kYXZ:
        {
            quat_rot =
                arclx::AxisAnglef(euler(1), arclx::Vector3f::UnitY()) *
                arclx::AxisAnglef(euler(0), arclx::Vector3f::UnitX()) *
                arclx::AxisAnglef(euler(2), arclx::Vector3f::UnitZ());
            break;
        }
        case RotationOrder::kYZX:
        {
            quat_rot =
                arclx::AxisAnglef(euler(1), arclx::Vector3f::UnitY()) *
                arclx::AxisAnglef(euler(2), arclx::Vector3f::UnitZ()) *
                arclx::AxisAnglef(euler(0), arclx::Vector3f::UnitX());
            break;
        }
        case RotationOrder::kZXY:
        {
            quat_rot =
                arclx::AxisAnglef(euler(2), arclx::Vector3f::UnitZ()) *
                arclx::AxisAnglef(euler(0), arclx::Vector3f::UnitX()) *
                arclx::AxisAnglef(euler(1), arclx::Vector3f::UnitY());
            break;
        }
        default:
        {
            quat_rot =
                arclx::AxisAnglef(euler(2), arclx::Vector3f::UnitZ()) *
                arclx::AxisAnglef(euler(1), arclx::Vector3f::UnitY()) *
                arclx::AxisAnglef(euler(0), arclx::Vector3f::UnitX());
            break;
        }
    }
    result.block<3, 3>(0, 0) = quat_rot.toRotationMatrix();
    return result;
}

/*!
 * \brief Produces a 4x4 affine matrix representing the given axis angle (in
 *        radians).
 */
inline arclx::Matrix44f rotate_axis_44f(
        float angle,
        arclx::Vector3f const& axis,
        RotationOrder order = RotationOrder::kZYX)
{
    arclx::Matrix44f result = arclx::Matrix44f::Identity();
    arclx::Quaternionf quat_rot;
    switch(order)
    {
        case RotationOrder::kXYZ:
        {
            quat_rot =
                arclx::AxisAnglef(
                    angle * axis(0),
                    arclx::Vector3f::UnitX()
                ) *
                arclx::AxisAnglef(
                    angle * axis(1),
                    arclx::Vector3f::UnitY()
                ) *
                arclx::AxisAnglef(
                    angle * axis(2),
                    arclx::Vector3f::UnitZ())
            ;
            break;
        }
        case RotationOrder::kXZY:
        {
            quat_rot =
                arclx::AxisAnglef(
                    angle * axis(0),
                    arclx::Vector3f::UnitX()
                ) *
                arclx::AxisAnglef(
                    angle * axis(2),
                    arclx::Vector3f::UnitZ()
                ) *
                arclx::AxisAnglef(
                    angle * axis(1),
                    arclx::Vector3f::UnitY())
            ;
            break;
        }
        case RotationOrder::kYXZ:
        {
            quat_rot =
                arclx::AxisAnglef(
                    angle * axis(1),
                    arclx::Vector3f::UnitY()
                ) *
                arclx::AxisAnglef(
                    angle * axis(0),
                    arclx::Vector3f::UnitX()
                ) *
                arclx::AxisAnglef(
                    angle * axis(2),
                    arclx::Vector3f::UnitZ())
            ;
            break;
        }
        case RotationOrder::kYZX:
        {
            quat_rot =
                arclx::AxisAnglef(
                    angle * axis(1),
                    arclx::Vector3f::UnitY()
                ) *
                arclx::AxisAnglef(
                    angle * axis(2),
                    arclx::Vector3f::UnitZ()
                ) *
                arclx::AxisAnglef(
                    angle * axis(0),
                    arclx::Vector3f::UnitX())
            ;
            break;
        }
        case RotationOrder::kZXY:
        {
            quat_rot =
                arclx::AxisAnglef(
                    angle * axis(2),
                    arclx::Vector3f::UnitZ()
                ) *
                arclx::AxisAnglef(
                    angle * axis(0),
                    arclx::Vector3f::UnitX()
                ) *
                arclx::AxisAnglef(
                    angle * axis(1),
                    arclx::Vector3f::UnitY())
            ;
            break;
        }
        default:
        {
            quat_rot =
                arclx::AxisAnglef(
                    angle * axis(2),
                    arclx::Vector3f::UnitZ()
                ) *
                arclx::AxisAnglef(
                    angle * axis(1),
                    arclx::Vector3f::UnitY()
                ) *
                arclx::AxisAnglef(
                    angle * axis(0),
                    arclx::Vector3f::UnitX())
            ;
            break;
        }
    }
    result.block<3, 3>(0, 0) = quat_rot.toRotationMatrix();
    return result;
}

/*!
 * \brief Produces a 4x4 affine matrix representing the given 3-dimensional
 *        scaling.
 */
inline arclx::Matrix44f scale_44f(arclx::Vector3f const& scaling)
{
    arclx::Matrix44f result;
    result
        << scaling(0),       0.0F,       0.0F, 0.0F,
                 0.0F, scaling(1),       0.0F, 0.0F,
                 0.0F,       0.0F, scaling(2), 0.0F,
                 0.0F,       0.0F,       0.0F, 1.0F;
    return result;
}

/*!
 * \brief Produces a 4x4 affine matrix representing the given uniform scaling.
 */
inline arclx::Matrix44f scale_44f(float scaling)
{
    return scale_44f(arclx::Vector3f(scaling, scaling, scaling));
}

/*!
 * \brief Computes and returns a 4x4 projection matrix.
 *
 * \param fov The horizontal field of view of the projection in radians.
 * \param aspect the horizontal aspect ration of the projection.
 * \param near_clip The distance of the near z clipping plane from the eye.
 * \param far_clip The distance of the far z clipping plane from the eye.
 */
inline arclx::Matrix44f perspective_44f(
        float fov,
        float aspect,
        float near_clip,
        float far_clip)
{
    float tan_half_fov = std::tan(fov / 2.0F);
    arclx::Matrix44f result = arclx::Matrix44f::Zero();
    result(0, 0) = 1.0F / (aspect * tan_half_fov);
    result(1, 1) = 1.0F / (tan_half_fov);
    result(2, 2) = -(far_clip + near_clip) / (far_clip - near_clip);
    result(3, 2) = -1.0F;
    result(2, 3) = -(2.0F * far_clip * near_clip) / (far_clip - near_clip);
    return result;
}

inline arclx::Matrix44f orthographic_44f(
        float left,
        float right,
        float bottom,
        float top,
        float near_clip,
        float far_clip)
{
    float width    =    right - left;
    float height   =      top - bottom;
    float distance = far_clip - near_clip;
    arclx::Matrix44f result;
    result
        << 2.0F / width,          0.0F,             0.0F, 0.0F,
                   0.0F, 2.0F / height,             0.0F, 0.0F,
                   0.0F,          0.0F, -2.0F / distance, 0.0F,
        -((right + left) / width),
        -((top + bottom) / height),
        -((far_clip + near_clip) / distance),
        1.0;
    return result;
}

/*!
 * \brief Extracts a 3-dimensional translation vector from the given 4x4 affine
 *        matrix.
 */
inline arclx::Vector3f extract_translation_from_affine_44f(
        const arclx::Matrix44f& affine)
{
    return arclx::Vector3f(
        affine(0, 3),
        affine(1, 3),
        affine(2, 3)
    );
}

/*!
 * \brief Extracts x, y, and z Euler rotation angles from the given 4x4
 *        affine matrix.
 */
inline void extract_euler_from_affine_44f(
        arclx::Matrix44f const& affine,
        float& out_yaw,
        float& out_pitch,
        float& out_roll)
{
    // TODO: just use exigent function for this

    out_yaw = std::atan2(affine(0, 1), affine(0, 0));
    out_pitch = std::atan2(
        -affine(0, 2),
        std::sqrt(
            (affine(1, 2) * affine(1, 2)) +
            (affine(2, 2) * affine(2, 2))
        )
    );
    out_roll = std::atan2(affine(1, 2), affine(2, 2));
}

} // namespace lx
} // namespace arc

#endif
