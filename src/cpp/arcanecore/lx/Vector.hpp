/*!
 * \file
 * \author David Saxon
 * \brief Vector types.
 *
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ARCANECORE_LX_VECTOR_HPP_
#define ARCANECORE_LX_VECTOR_HPP_

#include <arcanecore/base/hash/FNV.hpp>

#include "arcanecore/lx/LXAPI.hpp"
#include "arcanecore/lx/__eigen/Dense"


namespace arclx
{
ARCLX_VERSION_NS_BEGIN

/*!
 * \brief A 2-dimensional signed 32-bit integer vector type.
 */
typedef Eigen::Matrix<int32_t, 2, 1> Vector2i;

/*!
 * \brief A 3-dimensional signed 32-bit integer vector type.
 */
typedef Eigen::Matrix<int32_t, 3, 1> Vector3i;

/*!
 * \brief A 4-dimensional signed 32-bit integer vector type.
 */
typedef Eigen::Matrix<int32_t, 4, 1> Vector4i;

/*!
 * \brief A 2-dimensional unsigned 32-bit integer vector type.
 */
typedef Eigen::Matrix<uint32_t, 2, 1> Vector2u;

/*!
 * \brief A 3-dimensional unsigned 32-bit integer vector type.
 */
typedef Eigen::Matrix<uint32_t, 3, 1> Vector3u;

/*!
 * \brief A 4-dimensional unsigned 32-bit integer vector type.
 */
typedef Eigen::Matrix<uint32_t, 4, 1> Vector4u;

/*!
 * \brief A 2-dimensional floating point vector type.
 */
typedef Eigen::Matrix<float, 2, 1> Vector2f;

/*!
 * \brief A 3-dimensional floating point vector type.
 */
typedef Eigen::Matrix<float, 3, 1> Vector3f;

/*!
 * \brief A 4-dimensional floating point vector type.
 */
typedef Eigen::Matrix<float, 4, 1> Vector4f;

/*!
 * \brief A 2-dimensional double precision floating point vector type.
 */
typedef Eigen::Matrix<double, 2, 1> Vector2d;

/*!
 * \brief A 3-dimensional double precision floating point vector type.
 */
typedef Eigen::Matrix<double, 3, 1> Vector3d;

/*!
 * \brief A d-dimensional double precision floating point vector type.
 */
typedef Eigen::Matrix<double, 4, 1> Vector4d;

ARCLX_VERSION_NS_END
} // namespace arclx

//------------------------------------------------------------------------------
//                                     HASHES
//------------------------------------------------------------------------------

namespace std
{

// TODO: hash functions for other types

template<>
struct hash<arclx::Vector3i> :
    public unary_function<arclx::Vector3i, size_t>
{
    std::size_t operator()(const arclx::Vector3i& value) const
    {
        return static_cast<std::size_t>(arc::hash::fnv1a_64(&value(0), 12));
    }
};

} // namespace std

#endif
