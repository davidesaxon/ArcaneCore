/*!
 * \file
 * \author David Saxon
 * \brief Matrix types.
 *
 * \copyright Copyright (c) 2018, David Saxon
 *            All rights reserved.
 *
 * \license BSD 3-Clause License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef ARCANECORE_LX_MATRIX_HPP_
#define ARCANECORE_LX_MATRIX_HPP_

#include "arcanecore/lx/LXAPI.hpp"
#include "arcanecore/lx/__eigen/Dense"


namespace arclx
{
ARCLX_VERSION_NS_BEGIN

/*!
 * \brief Generic matrix type with without any templating applied.
 */
template<typename T_DataType, int T_Rows, int T_Cols>
using Matrix = Eigen::Matrix<T_DataType, T_Rows, T_Cols>;

/*!
 * \brief A 2x2 floating point matrix type.
 */
typedef Eigen::Matrix<float, 2, 2> Matrix22f;

/*!
 * \brief A 3x3 floating point matrix type.
 */
typedef Eigen::Matrix<float, 3, 3> Matrix33f;

/*!
 * \brief A 3x4 floating point matrix type.
 */
typedef Eigen::Matrix<float, 3, 4> Matrix34f;

/*!
 * \brief A 4x4 floating point matrix type.
 */
typedef Eigen::Matrix<float, 4, 4> Matrix44f;

/*!
 * \brief A 2x2 double precision floating point matrix type.
 */
typedef Eigen::Matrix<double, 2, 2> Matrix22d;

/*!
 * \brief A 3x3 double precision floating point matrix type.
 */
typedef Eigen::Matrix<double, 3, 3> Matrix33d;

/*!
 * \brief A 3x4 double precision floating point matrix type.
 */
typedef Eigen::Matrix<double, 3, 4> Matrix34d;

/*!
 * \brief A 4x4 double precision floating point matrix type.
 */
typedef Eigen::Matrix<double, 4, 4> Matrix44d;

ARCLX_VERSION_NS_END
} // namespace arclx

#endif
